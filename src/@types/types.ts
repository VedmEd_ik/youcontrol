import { BankDetailsType, PassportType } from 'src/redux/counterparty/types';

//========= тип документу, на підставі якого діє підписант ============//
export type BasisDocument = {
  [key: string]: { name: string; value: string };
};

//========= Тип області, де видавався паспорт ============//
export type RegionsPassport = {
  [key: string]: string;
};

//========= Тип списку договорів ============//
export type DocumentsList = string[];

//========= Типи даних моїх компаній ============//
export type FirmDataType = {
  [key: string]: MyCompanyType;
};

export type MyCompanyType = {
  fullName?: string;
  shortName?: string;
  address?: string;
  director?: string;
  code?: string;
  fullLegalForm?: string | null;
  shortLegalForm?: string | null;
  contacts?: {
    phone?: string | null;
    additionalPhone?: string | null;
    fax?: string | null;
    email?: string | null;
    webSite?: string | null;
    otherContacts?: string | null;
  };
  signer?: {
    name?: string | null;
    role?: string | null;
    basis?: string | null;
  };
  bankDetails?: BankDetailsType;
  taxationSystem?: string | null;
  PDV?: string | null;
  IPN?: string | null;
  passport?: PassportType;
};

//========= Тип строків оплати товару ============//
export type PaymentTerm = string[];
