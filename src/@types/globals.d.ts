declare module '*.module.css' {
  const classes: { [key: string]: string };
  export default classes;
}

declare module '*.module.scss' {
  const classes: { [key: string]: string };
  export default classes;
}

declare module '*.module.less' {
  const classes: { [key: string]: string };
  export default classes;
}

declare module '*.svg' {
  const content: any;
  export default content;
}

declare module '*.png' {
  const content: any;
  export default content;
}

declare global {
  namespace Express {
    export interface Request {
      user: auth.DecodedIdToken;
    }
  }
}

declare module '*.ttf' {
  const content: any;
  export default content;
}
