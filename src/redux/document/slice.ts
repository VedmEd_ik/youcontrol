import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DocumentSliceState } from '../document/types';

const initialState: DocumentSliceState = {
  documentData: {
    name: '',
    number: '',
    date: '',
    paymentTerm: '',
    fontSize: 9,
    status: false,
  },
};

export const documentSlice = createSlice({
  name: 'document',
  initialState,
  reducers: {
    setDocumentData(state, action: PayloadAction<DocumentSliceState['documentData']>) {
      state.documentData = { ...state.documentData, ...action.payload };
    },

    setStatusDocument(state) {
      if (
        state.documentData.name &&
        state.documentData.number &&
        state.documentData.date &&
        state.documentData.paymentTerm
      ) {
        state.documentData.status = true;
      } else {
        state.documentData.status = false;
      }
    },
  },
});

export const { setStatusDocument, setDocumentData } = documentSlice.actions;

export default documentSlice.reducer;
