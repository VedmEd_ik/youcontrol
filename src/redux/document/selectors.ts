import { RootState } from '../store';

export const statusDocumentSelector = (state: RootState) => state.documentSlice.documentData.status;

export const nameDocumentSelector = (state: RootState) => state.documentSlice.documentData.name;

export const fontSizeSelector = (state: RootState) => state.documentSlice.documentData.fontSize;

export const paymentTermSelector = (state: RootState) =>
  state.documentSlice.documentData.paymentTerm;

export const documentDataSelector = (state: RootState) => state.documentSlice.documentData;
