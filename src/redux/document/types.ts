export type DocumentDataType = {
  name?: string;
  number?: string;
  date?: string;
  paymentTerm?: string;
  status?: boolean;
  manager?: string;
  fontSize?: number;
};

export interface DocumentSliceState {
  documentData: DocumentDataType;
}
