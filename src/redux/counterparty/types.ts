export type CounterPartyType = {
  status?: boolean;
  isContract?: boolean;
  fullName?: string;
  shortName?: string;
  address?: string;
  postAddress?: string;
  director?: string;
  code?: string;
  fullLegalForm?: string;
  shortLegalForm?: string;
  contacts?: {
    phone?: string;
    additionalPhone?: string;
    fax?: string;
    email?: string;
    webSite?: string;
    otherContacts?: string;
  };
  signer?: {
    name?: string;
    role?: string;
    basis?: string;
    numberDocumentBasis?: string;
    dateDocumentBasis?: string;
  };
  bankDetails?: BankDetailsType;
  taxationSystem?: string;
  PDV?: string;
  IPN?: string | boolean;
  passport?: PassportType;
};

export type BankDetailsType = {
  bank1?: { 'bank-account'?: string; bank?: string };
  bank2?: { 'bank-account'?: string; bank?: string };
};

export type PassportType = {
  status?: string;
  number?: string;
  date?: string;
  organName?: string;
  organRegion?: string;
  passportType?: string;
};

type StatusRequesType = 'loading' | 'success' | 'error' | boolean;

export interface CounerpartySliceState {
  status: StatusRequesType;
  checkedCompany: DataFromAxios;
  errorCode: boolean;
  counterParty: CounterPartyType;
  statusCourt: StatusRequesType;
  courts: Court[];
  extractEDRLink: string;
  statusExtractEDR: StatusRequesType;
}

export type CourtResponseDataType = { count: number; items: Court[] };

export type CourtsResponse = {
  status: string;
  data: CourtResponseDataType;
};

export type Court = {
  doc_id: number;
  court_code: number;
  court_name: string;
  judgment_code: number;
  judgment_name: string;
  justice_code: number;
  justice_name: string;
  category_code: number;
  category_name: string;
  cause_number: string;
  adjudication_date: string;
  date_publ: string;
  receipt_date: string;
  judge: string;
  link: string;
};

type AddressType = {
  zip?: string;
  country?: string;
  address?: string;
  parts?: {
    atu?: string;
    street?: string;
    house?: string;
    building?: string;
    num?: string;
    houseType?: string;
    numType?: string;
    atuCode?: string;
  };
};

export type FactorItemsType = {
  number?: string;
  courtName?: string;
  category?: string;
  department?: string;
  type?: string;
  text?: string;
  count?: number;
  link?: string;
  liveCount?: number;
  decisionNumber?: string;
  decisionDate?: string;
  agency?: string;
  date?: string;
  court?: string;
  regAction?: string;
  members?: string;
  liquidatorName?: string;
  reorganizationType?: string;
  requirementDate?: string;
  pib?: string;
  declarantId?: string;
  years?: number[];
  changes?: {
    field?: string;
    record?: string;
    text?: string;
    oldValue?: string;
    newValue?: string;
  }[];
};

export type FactorType = {
  factorGroup?: string;
  type?: string;
  text?: string;
  icon?: string;
  indicator?: string;
  number?: string;
  status?: string;
  dateCancellation?: string;
  reasonCancellation?: string;
  agencyCancellation?: string;
  databaseDate?: string;
  dateEnd?: string;
  group?: number;
  rate?: string;
  total?: number;
  local?: number;
  government?: number;
  debt?: number;
  penaltiesCount?: string;
  link?: string;
  count?: number;
  deadCounter?: number;
  startDate?: string;
  dateStart?: string;
  endDate?: string;
  termless?: boolean;
  sanctionList?: string;
  sanctionReason?: string;
  sanctionComment?: string;
  ctottgCode?: string;
  occupationStatus?: boolean;
  blockadeStatus?: boolean;
  fightStatus?: boolean;
  evacuationStatus?: boolean;
  liberatedStatus?: boolean;
  excludedStatus?: boolean;
  updatedAt?: string;
  items?: FactorItemsType[];
};

export type DataFromAxios = {
  registry?: {
    fullName?: string;
    shortName?: string;
    fullNameEn?: string;
    shortNameEn?: string;
    code?: string;
    location?: string;
    email?: string;
    primaryActivity?: string;
    status?: string;
    ceoName?: string;
    phones?: string[];
    registrationDate?: string;
    capital?: number;
    lastTime?: string;
    heads?: {
      name?: string;
      role?: string;
      restriction?: string;
    }[];
    registrations?: {
      code?: string;
      name?: string;
      type?: number;
      startDate?: string;
      description?: string;
      endDate?: string;
    }[];
    beneficiaries?: {
      name?: string;
      role?: string;
      amount?: number;
      code?: number;
      location?: string;
      country?: string;
      amountPercent?: number;
    }[];
    fax?: string;
    webPage?: string;
    anotherInfo?: string;
    includeOlf?: number;
    state?: string;
    olfCode?: string;
    olfName?: string;
    olfSubtype?: string;
    foundingDocumentType?: number;
    foundingDocumentName?: string;
    executivePower?: {
      name?: string;
      code?: string;
    }[];
    objectName?: string;
    branches?: {
      name?: string;
      code?: string;
      address?: AddressType;
      id?: number;
      role?: number;
      roleText?: string;
    }[];
    authorisedCapital?: {
      value?: number;
      date?: string;
    };
    management?: string;
    managingPaper?: string;
    activities?: {
      code?: string;
      name?: string;
      isPrimary?: boolean;
    }[];
    address?: AddressType;
    registration?: {
      date?: string;
      recordNumber?: string;
      recordDate?: string;
      isSeparation?: boolean;
      isDivision?: boolean;
      isMerge?: boolean;
      isTransformation?: boolean;
    };
    bankruptcy?: {
      date?: string;
      state?: number;
      dateJudge?: string;
      stateText?: string;
      docDate?: string;
      docNumber?: string;
      courtName?: string;
    };
    termination?: {
      state?: number;
      stateText?: string;
      date?: string;
      recordNumber?: string;
      cause?: string;
    };
    terminationCancel?: {
      date?: string;
      recordNumber?: string;
      docDate?: string;
      courtName?: string;
      docNumber?: string;
      dateJudge?: string;
    };
    assignees?: {
      name?: string;
      code?: string;
      address?: AddressType;
    }[];
    predecessors?: {
      name?: string;
      code?: string;
      address?: AddressType;
    }[];
    primaryActivityKind?: {
      name?: string;
      code?: string;
      class?: string;
      regNumber?: string;
    };
    prevRegistrationEndTerm?: string;
    openEnforcements?: string[];
  };
  factors?: FactorType[];
  financialStatement?: {
    year?: number;
    revenue?: number;
    expenses?: number;
    profit?: number;
    nonCurrentAssets?: number;
    currentAssets?: number;
    liability?: number;
    balance?: number;
    sector?: string;
    buhgalter?: string;
  }[];
  registries?: {
    prozorro?: {
      name?: string;
      link?: string;
      count?: number;
      tenders?: {
        id?: string;
        registry?: string;
        title?: string;
        amount?: number;
        role?: string;
        buyer?: {
          code?: string;
          name?: string;
          legalName?: string;
          contactName?: string;
          contactEmail?: string;
          contactPhone?: string;
        }[];
        seller?: {
          name?: string;
          code?: string;
          contactName?: string;
          contactPhone?: string;
          contactEmail?: string;
        }[];
        status?: string;
        description?: string;
        updatedAt?: string;
        createdAt?: string;
        startDate?: string;
      }[];
    };
    smida?: {
      name?: string;
      text?: string;
      date?: string;
      year?: number;
      quarter?: number;
      shareholders?: {
        fullName?: string;
        code?: string;
        role?: string;
        country?: string;
        amountPercent?: number;
      }[];
    };
    licenses?: {
      gamingLicenses?: {
        name?: string;
        text?: string;
        licenseType?: string;
        licenseSubType?: string;
        date?: string;
        licenseNumber?: number;
        brand?: string;
        address?: string;
      }[];
      bigTaxpayers?: {
        name?: string;
        text?: string;
        year?: number;
      }[];
      diiaCity?: {
        name?: string;
        text?: string;
        decisionDate?: string;
        number?: number;
      }[];
    };
  };
  dpa?: {
    taxDepartments?: {
      taxDepartmentId?: number;
      regionCode?: number;
      districtCode?: number;
      districtName?: string;
      inspectionCode?: number;
      inspectionName?: string;
      inspectionRegistryCode?: number;
      code?: number;
      koatuuCode?: string;
      regionTaxDepartmentCode?: number;
    };
    taxRequisites?: [
      {
        type?: string;
        koatuuObl?: string;
        koatuu?: string;
        location?: string;
        recipient?: string;
        code?: number;
        bank?: string;
        mfo?: number;
        iban?: string;
        taxCode?: number;
      },
    ];
  };
  courtEntity?: {
    courtId?: number;
    name?: string;
    code?: string;
    stage?: string;
    type?: string;
  };
  link?: string;
};

export interface AxiosResponse {
  data?: {
    status?: string;
    forDevelopers?: string;
    data?: DataFromAxios;
  };
}
