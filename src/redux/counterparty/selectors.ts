import { RootState } from '../store';

export const counterPartySelector = (state: RootState) => state.counterPartySlice.counterParty;

export const statusCheckedCompanySelector = (state: RootState) => state.counterPartySlice.status;

export const statusCourtSelector = (state: RootState) => state.counterPartySlice.statusCourt;

export const courtsSelector = (state: RootState) => state.counterPartySlice.courts;

export const checkedCompanySelector = (state: RootState) => state.counterPartySlice.checkedCompany;

export const checkedCompanyCodeSelector = (state: RootState) =>
  state.counterPartySlice.checkedCompany.registry.code;

export const extractEDRSelector = (state: RootState) => state.counterPartySlice.extractEDRLink;

export const statusExtractEDRSelector = (state: RootState) =>
  state.counterPartySlice.statusExtractEDR;

export const isContractSelector = (state: RootState) =>
  state.counterPartySlice.counterParty.isContract;
