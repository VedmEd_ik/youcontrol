import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CounerpartySliceState, CounterPartyType } from '../counterparty/types';
import { fetchCompany, fetchCourtDecision, fetchExtractEDR } from './createAsyncThunks';

const initialState: CounerpartySliceState = {
  status: null,
  checkedCompany: null,
  errorCode: null,
  counterParty: {
    status: null,
    isContract: null,
    fullName: null,
    shortName: null,
    address: null,
    postAddress: null,
    director: null,
    code: null,
    fullLegalForm: null,
    shortLegalForm: null,
    contacts: {
      phone: null,
      additionalPhone: null,
      fax: null,
      email: null,
      webSite: null,
      otherContacts: null,
    },
    signer: {
      name: null,
      role: null,
      basis: null,
    },
    bankDetails: {
      bank1: {
        'bank-account': null,
        bank: null,
      },
      bank2: {
        'bank-account': null,
        bank: null,
      },
    },
    taxationSystem: null,
    PDV: null,
    IPN: null,
  },
  statusCourt: null,
  courts: null,
  extractEDRLink: null,
  statusExtractEDR: null,
};

export const counterPartySlice = createSlice({
  name: 'party',
  initialState,
  reducers: {
    setCounterParty(state, action: PayloadAction<CounterPartyType>) {
      state.counterParty = { ...state.counterParty, ...action.payload };
    },

    removeCounterparty(state) {
      state.counterParty = initialState.counterParty;
      state.checkedCompany = null;
      state.status = false;
    },
    clearCounterparty(state) {
      state.counterParty = initialState.counterParty;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCompany.pending, (state) => {
      console.log('Відправка запиту');
      state.status = 'loading';
      state.checkedCompany = null;
      state.extractEDRLink = null;
    });
    builder.addCase(fetchCompany.fulfilled, (state, action) => {
      console.log('Відповідь від сервера отримано');
      state.checkedCompany = action.payload;
      state.status = 'success';
    });
    builder.addCase(fetchCompany.rejected, (state) => {
      console.log('Помилка сервера');
      state.status = 'error';
      state.errorCode = false;
      state.checkedCompany = null;
      state.extractEDRLink = null;
    });
    builder.addCase(fetchCourtDecision.pending, (state) => {
      console.log('Відправка запиту на отримання судових рішень');
      state.statusCourt = 'loading';
      state.courts = null;
    });
    builder.addCase(fetchCourtDecision.fulfilled, (state, action) => {
      console.log('Судові рішення отримано', action.payload);
      state.courts = action.payload.items;
      state.statusCourt = 'success';
    });
    builder.addCase(fetchCourtDecision.rejected, (state, action) => {
      console.log('Сталася помилка при отриманні судових рішень від сервера', action.payload);
      state.statusCourt = 'error';
      state.errorCode = false;
      state.courts = null;
    });
    builder.addCase(fetchExtractEDR.pending, (state) => {
      console.log('Відправка запиту на отримання витягу');
      state.extractEDRLink = null;
      state.statusExtractEDR = 'loading';
    });
    builder.addCase(fetchExtractEDR.fulfilled, (state, action) => {
      console.log('Посилання на витяг отримано');
      console.log(action.payload.link);
      state.extractEDRLink = action.payload.link;
      state.statusExtractEDR = 'success';
    });
    builder.addCase(fetchExtractEDR.rejected, (state, action) => {
      console.log('Сталася помилка при отриманні посилання на витяг з ЄДР', action.payload);
      state.extractEDRLink = null;
      state.statusExtractEDR = 'error';
    });
  },
});

export const { setCounterParty, removeCounterparty, clearCounterparty } = counterPartySlice.actions;

export default counterPartySlice.reducer;
