import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';
import { checkCode } from 'src/utils/functions';
import { AxiosResponse, CourtResponseDataType, CourtsResponse, DataFromAxios } from './types';

// Axios запит на сервер для отримання інформації про компанію
export const fetchCompany = createAsyncThunk<DataFromAxios, string>(
  'party/fetchCompanyStatus',
  async (companyCode) => {
    let response: AxiosResponse = await axios.get(
      `https://api.youscore.com.ua/v1/usr/${companyCode}?showCurrentData=false&apiKey=${process.env.REACT_APP_API_KEY_YOUCONTROL}`,
    );

    console.log(response.data);
    // response = await axios.get(`/${companyCode}.json`);
    return response.data;
  },
);

export const fetchCourtDecision = createAsyncThunk<CourtResponseDataType, string>(
  'party/fetchCourt',
  async (link) => {
    const response = await axios.get<CourtsResponse>(link);
    // const response = await axios.get('./courtDecision.json');
    return response.data.data;
  },
);

export const fetchExtractEDR = createAsyncThunk<DataFromAxios, string>(
  'party/fetchExtract',
  async (companyCode) => {
    const response: AxiosResponse = await axios.get(
      `https://opendatabot.com/api/v2/pdf/${companyCode}?apiKey=${process.env.REACT_APP_API_KEY_OPENDATA}`,
    );
    return response.data.data;
  },
);
