import { MyCompanyType } from 'src/@types/types';

export interface MyCompanySliceState {
  myCompany: MyCompanyType;
}
