import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MyCompanySliceState } from '../myCompany/types';
import { MyCompanyType } from 'src/@types/types';

const initialState: MyCompanySliceState = {
  myCompany: {
    fullName: '',
    shortName: '',
    address: '',
    director: '',
    code: '',
    fullLegalForm: '',
    shortLegalForm: '',
    contacts: {
      phone: '',
      additionalPhone: '',
      fax: '',
      email: '',
      webSite: '',
      otherContacts: '',
    },
    signer: {
      name: '',
      role: '',
      basis: '',
    },
    bankDetails: {
      bank1: {
        'bank-account': null,
        bank: null,
      },
    },
    taxationSystem: '',
    PDV: '',
    IPN: '',
    passport: {
      status: '',
      number: '',
      date: '',
      organName: '',
      organRegion: '',
    },
  },
};

export const myCompanySlice = createSlice({
  name: 'company',
  initialState,
  reducers: {
    setMyCompany(state, action: PayloadAction<MyCompanyType>) {
      state.myCompany = action.payload;
    },
  },
});

export const { setMyCompany } = myCompanySlice.actions;

export default myCompanySlice.reducer;
