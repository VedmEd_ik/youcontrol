import { DocumentsList, FirmDataType, MyCompanyType, PaymentTerm } from 'src/@types/types';

export interface UserSliceState {
  user: UserType;
}

export type UserType = {
  id: string;
  name: string;
  email: string;
  token?: string;
  documentsList: DocumentsList;
  paymentTerm: PaymentTerm;
  firmData?: FirmDataType;
};
