import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { UserSliceState } from '../user/types';

const initialState: UserSliceState = {
  user: {
    id: null,
    name: null,
    email: null,
    token: null,
    documentsList: null,
    paymentTerm: null,
  },
};

export const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser(state, action: PayloadAction<UserSliceState['user']>) {
      console.log(action);
      state.user = { ...state.user, ...action.payload };
    },
    removeUser(state) {
      state.user = {
        id: null,
        name: null,
        email: null,
        token: null,
        documentsList: null,
        paymentTerm: null,
      };
      window.localStorage.removeItem('userData');
    },
  },
});

export const { setUser, removeUser } = userSlice.actions;

export default userSlice.reducer;
