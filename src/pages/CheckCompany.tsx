import React, { useState, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { ProgressBar } from 'react-loader-spinner';
import './CheckCompany.scss';
import { Link } from 'react-router-dom';
import { todayDate, formatDate, handleKeyDown, getFactor } from '../utils/functions';
import NavPanel from '../components/NavPanel/NavPanel';
import Error from '../components/Error/Error';
import { fetchCompany } from 'src/redux/counterparty/createAsyncThunks';
import {
  checkedCompanySelector,
  statusCheckedCompanySelector,
} from 'src/redux/counterparty/selectors';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { DataFromAxios, FactorType } from 'src/redux/counterparty/types';
import { useAppDispatch } from 'src/redux/store';

type FullInfoSatate = {
  [key: string]: { key: string; data: JSX.Element | JSX.Element[] | string };
};

const CheckCompany: React.FC = () => {
  const [companyCode, setCompanyCode] = useState('');
  const [fullInfo, setFullInfo] = useState<FullInfoSatate>({});
  const dispatch = useAppDispatch();
  const data = useSelector(checkedCompanySelector);
  const statusCheckedCompany = useSelector(statusCheckedCompanySelector);
  const companyInfo = useRef<HTMLElement>();

  const formatDataCounterpartyAndSetState = (data: DataFromAxios) => {
    const vat = getFactor('tax', 'vat', data);
    const singletax = getFactor('tax', 'singletax', data);
    const debt = getFactor('tax', 'debt', data);
    const wagedebt = getFactor('court', 'wagedebt', data);
    const courtDecision = getFactor('court', 'courtDecision', data);
    const courtCompany = getFactor('court', 'courtCompany', data);
    const penalty = getFactor('court', 'penalty', data);
    const amcu = getFactor('sanction', 'amcu', data);
    const bankruptcy = getFactor('edr', 'bankruptcy', data);
    const massAddress = getFactor('edr', 'massAddress', data);
    const history = getFactor('edr', 'history', data);
    const ruFounders = getFactor('edr', 'ruFounders', data);
    const byFounders = getFactor('edr', 'byFounders', data);
    const declarantOwner = getFactor('publicOfficial', 'declarantOwner', data);
    const sanction = getFactor('sanction', 'sanction', data);
    const branches = data?.registry?.branches;
    const companyCode = data?.registry?.code;

    const fullNameEn = data?.registry?.fullNameEn ? ' / ' + data.registry.fullNameEn : '';
    const shortNameEn = data?.registry?.shortNameEn ? ' / ' + data.registry.shortNameEn : '';

    setFullInfo({
      fullName: {
        key: 'Повне найменування',
        data:
          companyCode.length === 8
            ? data?.registry?.fullName + fullNameEn
            : companyCode.length > 8
            ? 'Фізична особа-підприємець ' + data?.registry?.fullName
            : '',
      },
      shortName: {
        key: 'Скорочене найменування',
        data:
          companyCode.length === 8
            ? data?.registry?.shortName
              ? data.registry.shortName + shortNameEn
              : 'Скорочене найменування відсутнє'
            : 'ФОП ' + data?.registry?.fullName,
      },
      code: { key: 'Код ЄДРПОУ', data: data?.registry?.code },
      status: {
        key: 'Статус',
        data:
          data?.registry?.status === 'зареєстровано' ? (
            <span>
              {data.registry.status} <span className="_icon-check-box_checked-2 _green" />
            </span>
          ) : (
            <span>
              {' '}
              {data?.registry?.status} <span className="_icon-cross _red" />
            </span>
          ),
      },
      registration: {
        key: 'Державна реєстрація',
        data: data?.registry?.registration
          ? `Зареєстровано в державному реєстрі ${formatDate(
              data?.registry?.registration?.date,
            )} року за номером ${data?.registry?.registration?.recordNumber}`
          : 'Немає даних',
      },
      location: { key: 'Місцезнаходження', data: data?.registry?.location },
      ceoName: {
        key: 'Директор',
        data: data?.registry?.ceoName ? data.registry.ceoName : 'Директор відсутній',
      },
      capital: {
        key: 'Статутний капітал, грн.',
        data: data?.registry?.capital
          ? data.registry.capital.toLocaleString('uk-UA')
          : 'Статутний капітал відсутній',
      },
      phones: {
        key: 'Телефони',
        data:
          data?.registry?.phones && data.registry.phones.length > 0
            ? data.registry.phones.map((el, index) => (
                <span key={el + index.toString()}> {el}; </span>
              ))
            : 'Телефони відсутні',
      },
      email: {
        key: 'Електронна адреса',
        data: data?.registry?.email ? data.registry.email : 'Електронна адреса відсутня',
      },
      heads: {
        key: 'Підписанти',
        data: data?.registry?.heads
          ? data.registry.heads.map((currrent, index) => {
              return (
                <div key={currrent + index.toString()}>
                  {currrent.name}, {currrent.role}{' '}
                  {currrent.restriction ? <span> {currrent.restriction}</span> : ''}
                </div>
              );
            })
          : 'Підписанти відсутні',
      },
      primaryActivity: { key: 'Основний КВЕД', data: data?.registry?.primaryActivity },
      activities: {
        key: 'КВЕДи',
        data: data?.registry?.activities
          ? data?.registry?.activities.map((current, index) => (
              <div key={current + index.toString()}>
                {`${current.code}. ${current.name}`}
                <br />
              </div>
            ))
          : 'Немає інформації про КВЕДи',
      },
      beneficiaries: {
        key: 'Бенефеціари',
        data: data?.registry?.beneficiaries
          ? data.registry.beneficiaries.map((current, index) => {
              return (
                <div key={current + index.toString()}>
                  <div>{current.name}</div>
                  <div>Статус: {current.role}</div>
                  {current.amountPercent ? (
                    <div>
                      Частка в статутному капіталі: {current.amountPercent}% - {current.amount} грн.
                    </div>
                  ) : (
                    ''
                  )}
                  {current.country ? <div>Країна: {current.country}</div> : ''}
                  {current.location ? <div>Адреса: {current.location}</div> : ''}
                  <br />
                </div>
              );
            })
          : 'Бенефеціари відсутні',
      },
      bankruptcy: {
        key: 'Процедури банкрутства',
        data:
          bankruptcy && bankruptcy.length > 0 ? (
            bankruptcy[0].items.length > 0 ? (
              <div>
                <div>
                  {bankruptcy[0].text} {bankruptcy[0].icon}
                </div>
                <br />
                {bankruptcy[0].items.map((current, index) => {
                  return (
                    <div key={current + index.toString()}>
                      <div>{current.text}</div>
                      <div>
                        Номер судової справи:{' '}
                        <a
                          href={current.link}
                          target="_blank"
                          rel="noreferrer"
                          className="check-company__content-link"
                        >
                          {current.court}
                        </a>
                      </div>
                      <div>Дата рішення: {formatDate(current.date)}</div>
                      <div>Номер публікації на сайті ВГСУ: {current.number}</div>
                      <br />
                    </div>
                  );
                })}
              </div>
            ) : (
              ''
            )
          ) : (
            'Процедур банкрутства щодо компанії не виявлено'
          ),
      },
      massAddress: {
        key: 'Місце масової реєстрації',
        data:
          massAddress && massAddress.length > 0 ? (
            <div>
              <div>Адреса компанії збігається з місцем масової реєстрації інших компаній.</div>
              <div>
                За цією адресою зареєстровано {massAddress[0].count} активних компаній та{' '}
                {massAddress[0].deadCounter} неактивних компаній
              </div>
            </div>
          ) : (
            'Компанія не знаходиться в місці масової реєстрації компаній'
          ),
      },
      courtCompany: {
        key: 'Судові процеси компанії',
        data:
          courtCompany && courtCompany.length > 0 ? (
            <div>
              <div>
                {courtCompany[0].text} - {courtCompany[0].count}
              </div>
              {courtCompany[0].items.map((current, index) => {
                return (
                  <div key={current.text + index}>
                    {current.text} - {current.count}
                  </div>
                );
              })}
            </div>
          ) : (
            'Немає судових процесів'
          ),
      },
      courtDecision: {
        key: 'Судові рішення',
        data:
          courtDecision && courtDecision.length > 0 ? (
            <Link to={'/court-decision'}>{courtDecision[0].text}</Link>
          ) : (
            'Немає судових рішень'
          ),
      },
      penalty: {
        key: 'Виконавчі провадження',
        data:
          penalty && penalty[0]?.items
            ? `Відкриті виконавчі провадження: ${penalty[0].count}`
            : 'Щодо компанії немає виконавчих проваджень',
      },
      amcu: {
        key: 'Санкції АМКУ',
        data:
          amcu && amcu[0]?.items
            ? `На компанію накладені санкції АМКУ в кількості - ${amcu[0].count}`
            : 'Компанія немає санкцій, накладених АМКУ',
      },
      sanction: {
        key: 'Санкції',
        data:
          sanction && sanction[0] ? (
            <div>
              <span>Санкційний лист: </span>
              <span>Документ, за яким накладено санкцію, {sanction[0]?.sanctionReason}.</span>
              <span>{sanction[0]?.sanctionComment}</span>
              <span>
                Строк дії санкції: з {sanction[0]?.startDate} по {sanction[0]?.endDate}
              </span>
            </div>
          ) : (
            'Немає санкцій'
          ),
      },
      ruFounders: {
        key: "Зв'язки з москалями",
        data:
          ruFounders && ruFounders[0]
            ? ruFounders[0].text
            : 'Компанія немає власників з Російської Федерації',
      },
      byFounders: {
        key: "Зв'язки з дерунами",
        data:
          byFounders && byFounders[0]
            ? byFounders[0].text
            : 'Компанія немає власників з Республіки Білорусь',
      },
      // =======================================================
      // branches: {
      //   key: 'Відокремлені підрозділи та філії',
      //   data:
      //     branches && branches.length > 0
      //       ? branches.map((current, index) => {
      //           return (
      //             <div key={current.name + index}>
      //               <div>{current.roleText}</div>
      //               <div>{current.name}</div>
      //               {current.code ? <div>Код ЄДРПОУ: {current.code}</div> : ''}
      //               {current.code ? <div>Адреса: {current.code}</div> : ''}
      //               <br />
      //             </div>
      //           );
      //         })
      //       : 'Компанія немає відокремлених підрозділів чи філій',
      // },
      // management: {
      //   key: 'Органи управління',
      //   data: data?.registry?.management ? data.registry.management : 'Органи управління відсутні',
      // },
      // systemTax: {
      //   key: 'Система оподаткування',
      //   data:
      //     singletax && singletax.length > 0 && singletax[0].status === 'active'
      //       ? 'Спрощена система оподаткування'
      //       : 'Загальна система оподаткування',
      // },
      // ПДВ?
      // vat: {
      //   key: 'ПДВ',
      //   data:
      //     vat && vat.length > 0
      //       ? vat.map((current, index) => {
      //           return (
      //             <div key={current.text + index.toString()}>
      //               <div>{current.text}</div>
      //               <div>
      //                 Статус:{' '}
      //                 {current.status === 'active' ? (
      //                   <span>
      //                     Активний <span className="_icon-check-box_checked-2 _green" />
      //                   </span>
      //                 ) : (
      //                   <span>
      //                     Неактивний <span className="_icon-cross _red" />
      //                   </span>
      //                 )}
      //               </div>
      //               <div>ІПН: {current.number}</div>
      //               <div>
      //                 Дата реєстрації платником податку: {formatDate(current.dateStart)} року
      //               </div>
      //               {current.dateCancellation ? (
      //                 <div>
      //                   {current.agencyCancellation} {formatDate(current.dateCancellation)}
      //                 </div>
      //               ) : (
      //                 ''
      //               )}
      //             </div>
      //           );
      //         })
      //       : 'Не платник податку',
      // },
      // Єдиний податок?
      // singletax: {
      //   key: 'Єдиний податок',
      //   data:
      //     singletax && singletax.length > 0
      //       ? singletax.map((current, index) => {
      //           return (
      //             <div key={current.text + index}>
      //               <div>{current.text}</div>
      //               <div>
      //                 Статус:{' '}
      //                 {current.status === 'active' ? (
      //                   <span>
      //                     Активний <span className="_icon-check-box_checked-2 _green" />
      //                   </span>
      //                 ) : (
      //                   <span>
      //                     Неактивний <span className="_icon-cross _red" />
      //                   </span>
      //                 )}
      //               </div>
      //               <div>
      //                 Дата реєстрації платником податку: {formatDate(current.dateStart)} року
      //               </div>
      //               {current.dateEnd && current.dateEnd !== 'null' ? (
      //                 <div>Дата припинення: {formatDate(current.dateEnd)} року</div>
      //               ) : (
      //                 ''
      //               )}
      //             </div>
      //           );
      //         })
      //       : 'Не платник податку',
      // },
      // debt: {
      //   key: 'Податковий борг',
      //   data:
      //     debt && debt.length > 0 && debt[0]?.government
      //       ? debt[0].text
      //       : `Станом на ${todayDate()} року податковий борг відсутній`,
      // },
      // wagedebt: {
      //   key: 'Борг, визначений судом',
      //   data:
      //     wagedebt && wagedebt.length > 0
      //       ? `Станом на ${todayDate()} року ${wagedebt[0].text.toLowerCase()}`
      //       : `Станом на ${todayDate()} року борг відсутній`,
      // },
      // history: {
      //   key: 'Зміни в реєстрі',
      //   data:
      //     history && history[0]?.items?.length > 0
      //       ? history[0].items.map((current, index) => {
      //           return (
      //             <div key={current + index.toString()}>
      //               <div>Дата: {formatDate(current.date)} року</div>
      //               {current.changes.map((change, index) => {
      //                 return (
      //                   <div key={change + index.toString()}>
      //                     <div>{change.text}</div>
      //                     <div>Було: {change.oldValue}</div>
      //                     {change.newValue ? <div>Стало: {change.newValue}</div> : ''}
      //                   </div>
      //                 );
      //               })}
      //               <br />
      //             </div>
      //           );
      //         })
      //       : 'В ЄДР не зафіксовано жодних змін',
      // },
      // declarantOwner: {
      //   key: 'Публічні особи в засновниках',
      //   data:
      //     declarantOwner && declarantOwner.length > 0
      //       ? declarantOwner[0].items.map((current, index) => {
      //           return (
      //             <div key={current + index.toString()}>
      //               <div>{current.pib}</div>
      //               <div>ІД декларанта {current.declarantId}</div>
      //               <div>
      //                 Роки декларування: {current.years.reduce((acc, cur) => acc + cur + ', ', '')}
      //               </div>
      //               <br />
      //             </div>
      //           );
      //         })
      //       : 'Засновники компанії не є публічними особами',
      // },
      // smida: {
      //   key: 'Інформація щодо акціонерів компанії',
      //   data: data?.registries?.smida ? (
      //     <div>
      //       <div>{data.registries.smida.name}</div>
      //       <div>
      //         Станом на {formatDate(data.registries.smida.date)} року (
      //         {data.registries.smida.quarter} квартал {data.registries.smida.year} року) компанія
      //         має акціонерів у кількості - {data.registries.smida.shareholders.length}, а саме:
      //       </div>
      //       <br />
      //       {data.registries.smida.shareholders.length > 0
      //         ? data.registries.smida.shareholders.map((current, index) => {
      //             return (
      //               <div key={current + index.toString()}>
      //                 <div>{current.fullName}</div>
      //                 <div>Відсоток акцій: {current.amountPercent}</div>
      //                 <div>Країна: {current.country}</div>
      //               </div>
      //             );
      //           })
      //         : ''}
      //       <br />
      //     </div>
      //   ) : (
      //     'Компанія немає акціонерів'
      //   ),
      // },
      // registrations: {
      //   key: 'Органи, які провели реєстрацію компанії',
      //   data: data?.registry?.registrations
      //     ? data.registry.registrations.map((current, index) => {
      //         return (
      //           <div key={current + index.toString()}>
      //             {current.name}
      //             <div>Код ЄДРПОУ органу: {current.code}</div>
      //             {current.description ? <div>{current.description}</div> : ''}
      //             <div>Дата реєстрації: {formatDate(current.startDate)} року</div>
      //             <br />
      //           </div>
      //         );
      //       })
      //     : 'Інформація відсутня',
      // },
      // licenses: { key: 'Ліцензії', data: data?.registries },
    });
  };

  const setDataCompanyToRedux = () => {
    if (data && data.registry.code === companyCode) {
      const info = {
        isContract: true,
        fullName:
          companyCode.length === 8
            ? data.registry.fullName
            : 'Фізична особа-підприємець ' + data?.registry?.fullName,
        shortName:
          companyCode.length === 8
            ? data?.registry?.shortName
              ? data.registry.shortName
              : data.registry.fullName
            : 'ФОП ' + data?.registry?.fullName,
        address: data.registry.address.address,
        director: data?.registry?.ceoName ? data.registry.ceoName : data.registry.fullName,
        signer: {
          name:
            data?.registry?.code.length === 8
              ? data?.registry?.heads[0].name
              : data.registry.fullName,
          role: data?.registry?.code.length === 8 ? 'Директор' : 'Керівник',
          basis: data?.registry?.code.length === 8 ? 'Статут' : 'Виписка з ЄДР',
        },
        code: data.registry.code,
        fullLegalForm: data.registry.olfName,
        shortLegalForm: '',
        contacts: {
          phone: data.registry.phones
            ? data.registry.phones.reduce((phones, phone) => phones + phone + ', ', '')
            : null,
          email: data.registry.email ? data.registry.email : null,
        },
        taxationSystem: taxInfo(data.factors)?.taxationSystem,
        PDV: taxInfo(data.factors)?.PDV,
        IPN: taxInfo(data.factors)?.IPN,
      };

      dispatch(setCounterParty(info));
    }
  };

  useEffect(() => {
    if (data) formatDataCounterpartyAndSetState(data);
  }, [data]);

  const handleCheck = () => {
    dispatch(fetchCompany(companyCode));
  };

  const taxInfo = (factorsArr: FactorType[]) => {
    const info = {
      taxationSystem: '',
      PDV: '',
      IPN: '',
    };

    if (factorsArr.length > 0) {
      const isSingletax = factorsArr.find(
        (current) =>
          current.factorGroup === 'tax' &&
          current.type === 'singletax' &&
          current.status === 'active',
      );

      info.taxationSystem = isSingletax
        ? `Платник єдиного податку ${isSingletax.group} групи`
        : 'Платник податку на загальній системі';

      const isPDV = factorsArr.find(
        (current) =>
          current.factorGroup === 'tax' && current.type === 'vat' && current.status === 'active',
      );

      info.PDV = isPDV ? `з реєстрацією ПДВ` : 'без реєстрації ПДВ';
      info.IPN = isPDV ? isPDV.number : '';

      return info;
    }
  };

  return (
    <section className="check-company">
      <div className="check-company__header">
        <h1 className="check-company__title _title">Перевірка контрагента</h1>
        <input
          type="text"
          onChange={(event) => setCompanyCode(event.target.value)}
          value={companyCode}
          className="check-company__input _input"
          placeholder="Введіть код ЄДРПОУ"
          onKeyDown={(event) => {
            handleKeyDown(event, handleCheck);
          }}
        />
        <button type="button" onClick={handleCheck} className="check-company__button-check _button">
          Перевірити
        </button>
      </div>

      {statusCheckedCompany === 'success' ? (
        <>
          <NavPanel
            documenTitleToPrint={`${fullInfo?.fullName?.data}`}
            refComponentToPrint={companyInfo}
          />
          <section ref={companyInfo} className="check-company__content">
            <h2>
              {fullInfo?.fullName?.data} <br />
              (дані взято з OpendataUA)
            </h2>
            <ul>
              {Object.keys(fullInfo).map((key, index) => {
                return (
                  <li key={key + index}>
                    <div>{fullInfo[key].key}:</div>
                    <div>{fullInfo[key].data}</div>
                  </li>
                );
              })}
            </ul>
          </section>

          <div className="check-company__footer">
            <Link to="/contract">
              <button
                type="button"
                onClick={setDataCompanyToRedux}
                className="check-company__button-contract _button"
              >
                Укласти договір
              </button>
            </Link>
          </div>
        </>
      ) : statusCheckedCompany === 'loading' ? (
        <ProgressBar
          height="80"
          width="80"
          ariaLabel="progress-bar-loading"
          wrapperStyle={{}}
          wrapperClass="progress-bar-wrapper"
          borderColor="#F4442E"
          barColor="#51E5FF"
        />
      ) : statusCheckedCompany === 'error' ? (
        <Error text={'Виникла помилка'} />
      ) : (
        ''
      )}
    </section>
  );
};

export default CheckCompany;
