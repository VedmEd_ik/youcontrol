import React, { useState, useEffect, memo } from 'react';
import { useSelector } from 'react-redux';
import {
  documentDataSelector,
  nameDocumentSelector,
  statusDocumentSelector,
} from 'src/redux/document/selectors';
import { CounterpartySection, MainInfoDocumentSection } from '../components';
import OutDocumentVerest from '../companies/Verest/OutDocument';
import useAuth from 'src/hooks/use-auth';
import MyCompanySection from 'src/components/MyCompanySection';

import './Contract.scss';
import { counterPartySelector } from 'src/redux/counterparty/selectors';
import { myCompanySelector } from 'src/redux/myCompany/selectors';
import { CounterPartyType } from 'src/redux/counterparty/types';
import { DocumentDataType } from 'src/redux/document/types';
import { MyCompanyType } from 'src/@types/types';

const Contract: React.FC = memo(() => {
  const dataStatus = useSelector(statusDocumentSelector);

  const [generatedDocument, setGeneratedDocument] = useState(false);
  const [isDataUpdate, setIsDataUpdate] = useState(false);

  const { email } = useAuth();
  const counterparty = useSelector(counterPartySelector);
  const dataDocument = useSelector(documentDataSelector);
  const myCompany = useSelector(myCompanySelector);

  let counterpartyCopy: CounterPartyType;
  let dataDocumentCopy: DocumentDataType;
  let myCompanyCopy: MyCompanyType;

  const handleGenDoc = () => {
    counterpartyCopy = JSON.parse(JSON.stringify(counterparty));
    dataDocumentCopy = JSON.parse(JSON.stringify(dataDocument));
    myCompanyCopy = JSON.parse(JSON.stringify(myCompany));
    setGeneratedDocument(true);
    setIsDataUpdate(true);
  };

  const checkData = () => {
    if (
      counterparty === counterpartyCopy &&
      dataDocument === dataDocumentCopy &&
      myCompany === myCompanyCopy
    ) {
      setIsDataUpdate(true);
    } else {
      setIsDataUpdate(false);
    }
  };

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  useEffect(() => {
    console.log('Зміни в counterparty, dataDocument, myCompany');
    checkData();
  }, [counterparty, dataDocument, myCompany]);

  const outDocument = (email: string) => {
    switch (email) {
      case 'verest.juryst@gmail.com':
        return (
          <OutDocumentVerest
            counterparty={counterparty}
            dataDocument={dataDocument}
            myCompany={myCompany}
          />
        );
      case 'edik171989@gmail.com':
        return <></>;
    }
  };

  console.log('Рендер компоненту Contract');

  return (
    <>
      <section className="input-data">
        <MainInfoDocumentSection />

        <MyCompanySection />

        <CounterpartySection />

        {/* Кнопка */}
        <div className="_red _bold">* - Поле має бути обов'зяково заповнене</div>
        <input
          type="button"
          className="generate-document-button _button _icon-document"
          disabled={dataStatus ? false : true}
          value={'Згенерувати документ'}
          onClick={() => {
            console.log('Спрацювала кнопка Згенерувати документ');
            handleGenDoc();
          }}
        />
      </section>

      <section className="output-data">
        {generatedDocument && isDataUpdate ? (
          outDocument(email)
        ) : (
          <div>Тут буде згенеровано ваш документ</div>
        )}
      </section>
    </>
  );
});

export default Contract;
