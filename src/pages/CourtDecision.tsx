import React, { useEffect, useState } from 'react';
import './CourtDecision.scss';
import { formatDate, getFactor } from '../utils/functions';
import { Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { fetchCourtDecision } from 'src/redux/counterparty/createAsyncThunks';
import { courtsSelector, checkedCompanySelector } from 'src/redux/counterparty/selectors';
import { useAppDispatch } from 'src/redux/store';

const CourtDecision: React.FC = () => {
  const dispatch = useAppDispatch();
  const decisions = useSelector(courtsSelector);
  const checkedCompany = useSelector(checkedCompanySelector);
  const [linkToDecisions, setLinkToDecisions] = useState('');

  useEffect(() => {
    if (checkedCompany) {
      const { link } = getFactor('court', 'courtDecision', checkedCompany)[0];
      setLinkToDecisions(link);
    }
  }, [checkedCompany]);

  useEffect(() => {
    if (linkToDecisions) {
      dispatch(fetchCourtDecision(linkToDecisions));
    }
  }, [linkToDecisions]);

  if (!checkedCompany) {
    return <Navigate to="/check-company" replace />; //
  }

  return (
    <div className="court-decision-wrap">
      <h1>Судові рішення компанії</h1>
      {decisions ? (
        <section className="court-decisions">
          {decisions && [decisions].length > 0 ? (
            decisions.map((decision, index) => {
              return (
                <section className="court-decision" key={decision.judgment_name + index.toString()}>
                  <a href={`${decision.link}`} target="_blank" rel="noreferrer">
                    <div className="court-decision__justice">{decision.justice_name}</div>
                    <div className="court-decision__name-court">{decision.court_name}</div>
                    <div className="court-decision__category">
                      Судочинство: {decision.judgment_name}
                    </div>
                    <div className="court-decision__number">Справа №: {decision.cause_number}</div>
                    <div className="court-decision__adjudication-date">
                      {formatDate(decision.adjudication_date)}
                    </div>
                  </a>
                </section>
              );
            })
          ) : (
            <div>Судові рішення відсутні</div>
          )}
        </section>
      ) : (
        ''
      )}
    </div>
  );
};

export default CourtDecision;
