import { CounterPartyType, DataFromAxios, FactorType } from 'src/redux/counterparty/types';
import qrcode from 'qrcode';

// Функція, яка форматує дату типу "2022-03-21" на "21.03.2022"
export const formatDate = (date: string) => {
  const newDate = new Date(date);
  const day = newDate.getDate() < 10 ? '0' + newDate.getDate() : newDate.getDate();
  const month =
    newDate.getMonth() + 1 < 10 ? '0' + (newDate.getMonth() + 1) : newDate.getMonth() + 1;
  const year = newDate.getFullYear();
  return day + '.' + month + '.' + year;
};

// Функція, яка відділяє перше слово "Договір" від решти назви документу
export const parseNameDocument = (fullNameDocument: string) => {
  const arr = fullNameDocument.split(' ');
  const document = arr[0];
  const nameDocument = arr.slice(1).reduce((acc, word) => acc + ' ' + word, '');
  return { document, nameDocument };
};

// Функція, яка перевіряє код ЄДРПОУ - чи ЮО чи ФОП
export const checkCode = (code: string) => {
  const regFOP = /\d{10}/;
  const regCompany = /\d{8}/;
  if (code.length === 8 && code.match(regCompany)) {
    return 'Company';
  } else if (code.length === 10 && code.match(regFOP)) {
    return 'FOP';
  }
};

// Функція, яка повертає сьогоднішню дату у формат: "21.03.2022"
export const todayDate = (isFormat = true) => {
  const date = new Date();
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const yy = date.getFullYear();

  let dd = String(day);
  let mm = String(month);

  if (day < 10) dd = '0' + day;

  if (month < 10) mm = '0' + month;

  return isFormat ? `${dd}.${mm}.${yy}` : `${yy}-${mm}-${dd}`;
};

// Функція, яка відслідковує натискання клавіші Enter
export const handleKeyDown = (event: React.KeyboardEvent, handleFunc: () => void) => {
  if (event.key.toLowerCase() === 'enter') {
    handleFunc();
  }
};

// Функція, яка робить першу літеру прописною
export const capitalize = (str: string) => str.charAt(0).toUpperCase() + str.slice(1);

// Функція, яка витягає необхідний ризик-фактор
export const getFactor = (factorGroup: string, type: string, data: DataFromAxios): FactorType[] => {
  if (data.factors) {
    const inf = data.factors.filter(
      (current) => current.factorGroup === factorGroup && current.type === type,
    );
    return inf;
  }
};

export const documentSignerBasis = (counterparty: CounterPartyType) => {
  if (counterparty.signer.basis === 'Довіреність') {
    return `${counterparty.signer.basis} № ${
      counterparty.signer.numberDocumentBasis
    } від ${formatDate(counterparty.signer.dateDocumentBasis)} року`;
  } else {
    return counterparty.signer.basis;
  }
};

// Функція, яка генерує qr-код
export const genQrCode = async (valueString: string) => {
  const qrCodeImage = await qrcode.toDataURL(valueString);
  return qrCodeImage;
};

// Функція, яка записує дані в session storage
export const setDataToSessionStorage = (
  key: 'counterparty' | 'myCompany' | 'mainInfoDocument',
  data: any,
) => {
  const getDataSessionStorage = sessionStorage.getItem(key);

  if (getDataSessionStorage) {
    const newData = JSON.stringify({ ...JSON.parse(getDataSessionStorage), ...data });
    sessionStorage.setItem(key, newData);
  } else {
    sessionStorage.setItem(key, JSON.stringify(data));
  }
};

// Функція, яка повертає правильне написання порядку оплати в договорі
export const paymentProcedure = (payment: string, contractObject: 'товар' | 'послуги') => {
  if (contractObject === 'товар') {
    switch (payment) {
      case 'шляхом 100% передоплати':
        return payment;
      case 'по факту':
        return `${payment} поставки Товару`;
      default:
        return `${payment} з моменту поставки Товару`;
    }
  } else if (contractObject === 'послуги') {
    switch (payment) {
      case 'шляхом 100% передоплати':
        return payment;
      case 'по факту':
        return `${payment} надання Послуг`;
      default:
        return `${payment} з моменту надання Послуг`;
    }
  }
};
