import { useLocation } from 'react-router-dom';
import { Logo, LogIn, Back } from '../';
import './Header.scss';

const Header: React.FC = () => {
  const { pathname } = useLocation();

  return (
    <section className="header">
      <Logo />
      <div className="header__panel">
        {pathname !== '/' && pathname !== '/check-company' && <Back />}
        <LogIn />
      </div>
    </section>
  );
};

export default Header;
