import React, { useEffect, useState } from 'react';
import ListRadiobuttons from '../ListRadiobuttons';
import { setDataToSessionStorage } from 'src/utils/functions';
import { useSelector } from 'react-redux';
import { counterPartySelector } from 'src/redux/counterparty/selectors';
import { useAppDispatch } from 'src/redux/store';
import { setCounterParty } from 'src/redux/counterparty/slice';

interface CounterpartySignerProps {}

type Signer = {
  mainSigner: string;
  mainRole: string;
  isOtherSigner: boolean;
  otherSignerName: string;
  otherSignerRole: string;
};

const CounterpartySigner: React.FC<CounterpartySignerProps> = ({}) => {
  const { director, code } = useSelector(counterPartySelector);

  const [signerState, setSignerState] = useState<Signer>({
    mainSigner: '',
    mainRole: code?.length === 8 ? 'Директор' : 'Керівник',
    isOtherSigner: false,
    otherSignerRole: '',
    otherSignerName: '',
  });

  const { signer } = useSelector(counterPartySelector);

  const dispatch = useAppDispatch();

  useEffect(() => {
    setSignerState({ ...signerState, mainSigner: director });
  }, []);

  useEffect(() => {
    dispatch(
      setCounterParty({
        signer: {
          ...signer,
          name: signerState.isOtherSigner ? signerState.otherSignerName : signerState.mainSigner,
          role: signerState.isOtherSigner ? signerState.otherSignerRole : signerState.mainRole,
        },
      }),
    );
  }, [signerState]);

  const handleSigner = (value: string) => {
    if (value === 'Директор/ФОП особисто') {
      setSignerState({
        mainSigner: director,
        mainRole: code.length === 8 ? 'Директор' : 'Керівник',
        isOtherSigner: false,
        otherSignerRole: '',
        otherSignerName: '',
      });
    } else if (value === 'Інша особа') {
      setSignerState({
        ...signerState,
        mainSigner: '',
        mainRole: '',
        isOtherSigner: true,
      });
    }
  };

  return (
    <section className="choise-counterparty__signer _section">
      <h2 className="choise-counterparty__signer-label _red">Підписант*</h2>

      <ListRadiobuttons
        arrValues={['Директор/ФОП особисто', 'Інша особа']}
        nameValue="counterpartySigner"
        getValueRadiobutton={handleSigner}
        indexDefaultValue={0}
      />

      {signerState.isOtherSigner && (
        <>
          <input
            type="text"
            placeholder="Введіть повне ПІБ підписанта"
            className="_input"
            name="signerOtherName"
            value={signerState.otherSignerName}
            onChange={(event) =>
              setSignerState({ ...signerState, otherSignerName: event.target.value })
            }
          />
          <input
            type="text"
            placeholder="Введіть посаду підписанта"
            className="_input"
            name="signerOtherRole"
            value={signerState.otherSignerRole}
            onChange={(event) =>
              setSignerState({ ...signerState, otherSignerRole: event.target.value })
            }
          />
        </>
      )}
    </section>
  );
};

export default CounterpartySigner;
