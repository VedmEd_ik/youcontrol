import React, { useEffect, useState } from 'react';
import { basisDocument2 } from 'src/assets/data';
import ListRadiobuttons from '../ListRadiobuttons';
import { formatDate, setDataToSessionStorage } from 'src/utils/functions';
import { useAppDispatch } from 'src/redux/store';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { useSelector } from 'react-redux';
import { counterPartySelector } from 'src/redux/counterparty/selectors';

interface CounterpartyBasicDocProps {}

type SingerBasisDocType = {
  nameDoc: string;
  dateDoc?: string;
  numberDoc?: string;
  isOtherBasisDoc: boolean;
};

const basisDocsArr = ['Статут/Виписка з ЄДР', 'Інший документ'];

const CounterpartyBasicDoc: React.FC<CounterpartyBasicDocProps> = ({}) => {
  const [singerBasisDoc, setSingerBasisDoc] = useState<SingerBasisDocType>({
    nameDoc: basisDocument2[1],
    isOtherBasisDoc: false,
  });

  const { code, signer } = useSelector(counterPartySelector);

  const dispatch = useAppDispatch();

  useEffect(() => {
    let basis = '';
    const nameDoc = singerBasisDoc.nameDoc;
    const numberDoc = singerBasisDoc.numberDoc;
    const dateDoc = singerBasisDoc.dateDoc ? formatDate(singerBasisDoc.dateDoc) : '';

    if (nameDoc && numberDoc && dateDoc) {
      basis = `${nameDoc} № ${numberDoc} від ${dateDoc} року`;
    } else if (nameDoc && numberDoc && !dateDoc) {
      basis = `${nameDoc} № ${numberDoc}`;
    } else if (nameDoc && !numberDoc && dateDoc) {
      basis = `${nameDoc} від ${dateDoc} року`;
    } else if (nameDoc && !numberDoc && !dateDoc) {
      basis = `${nameDoc}`;
    } else {
      basis = `______________________________`;
    }

    dispatch(
      setCounterParty({
        signer: {
          ...signer,
          basis: basis,
          numberDocumentBasis: singerBasisDoc.numberDoc ? singerBasisDoc.numberDoc : null,
          dateDocumentBasis: singerBasisDoc.dateDoc ? formatDate(singerBasisDoc.dateDoc) : null,
        },
      }),
    );
  }, [singerBasisDoc]);

  const handleSignerBasicDoc = (value: string) => {
    if (value === 'Статут/Виписка з ЄДР') {
      setSingerBasisDoc({
        ...singerBasisDoc,
        nameDoc: code.length === 8 ? 'Статут' : 'Виписка з ЄДР',
        dateDoc: '',
        numberDoc: '',
        isOtherBasisDoc: false,
      });
    } else if (value === 'Інший документ') {
      setSingerBasisDoc({
        nameDoc: '',
        dateDoc: '',
        numberDoc: '',
        isOtherBasisDoc: true,
      });
    }
  };

  return (
    <section className="choise-counterparty__basis _section">
      <h2 className="choise-counterparty__basis-label _red">
        Документ, на підставі якого діє підписант*
      </h2>

      <ListRadiobuttons
        nameValue="basisDocument"
        getValueRadiobutton={handleSignerBasicDoc}
        arrValues={basisDocsArr}
        indexDefaultValue={0}
      />

      {singerBasisDoc.isOtherBasisDoc && (
        <>
          <input
            type="text"
            placeholder="Введіть назву документу"
            className="choise-counterparty__basis-authority-number _input"
            value={singerBasisDoc.nameDoc}
            onChange={(event) =>
              setSingerBasisDoc({ ...singerBasisDoc, nameDoc: event.target.value })
            }
          />
          <input
            type="text"
            placeholder="Введіть номер документу"
            className="choise-counterparty__basis-authority-number _input"
            value={singerBasisDoc.numberDoc}
            onChange={(event) =>
              setSingerBasisDoc({ ...singerBasisDoc, numberDoc: event.target.value })
            }
          />
          <input
            type="date"
            placeholder="Введіть дату документу"
            className="choise-counterparty__basis-authority-date _input"
            value={singerBasisDoc.dateDoc}
            onChange={(event) =>
              setSingerBasisDoc({ ...singerBasisDoc, dateDoc: event.target.value })
            }
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyBasicDoc;
