import React, { useEffect, useState } from 'react';
import ListRadiobuttons from '../ListRadiobuttons';
import { documentsListVerest, managers } from 'src/companies/Verest/verestData';
import { todayDate } from 'src/utils/functions';
import { setDocumentData, setStatusDocument } from 'src/redux/document/slice';
import { useAppDispatch } from 'src/redux/store';
import { fontSizes, paymentTerm } from 'src/assets/data';

type MainInfoDocStateType = {
  nameDocument: string;
  dataDocument: string;
  numberDocument: string;
  payDocument: string;
};

const MainInfoDocumentSection: React.FC = ({}) => {
  const dispatch = useAppDispatch();

  const [fontSize, setFontSize] = useState(9);

  const [mainInfoDocState, setMainInfoDocState] = useState<MainInfoDocStateType>({
    nameDocument: documentsListVerest[0],
    dataDocument: todayDate(false),
    numberDocument: '',
    payDocument: paymentTerm[2],
  });

  const [manager, setManager] = useState('');

  const setNameDoc = (value: string) => {
    setMainInfoDocState({ ...mainInfoDocState, nameDocument: value });
  };

  const setPayDocument = (value: string) => {
    if (!paymentTerm.includes(value)) {
      setMainInfoDocState({
        ...mainInfoDocState,
        payDocument: `протягом ${value} календарних днів`,
      });
    } else {
      setMainInfoDocState({
        ...mainInfoDocState,
        payDocument: value,
      });
    }
  };

  useEffect(() => {
    const infoDocument = {
      name: mainInfoDocState.nameDocument,
      number: mainInfoDocState.numberDocument,
      date: mainInfoDocState.dataDocument,
      paymentTerm: mainInfoDocState.payDocument,
      manager: manager,
      fontSize: fontSize,
    };
    dispatch(setDocumentData(infoDocument));
    dispatch(setStatusDocument());
  }, [mainInfoDocState, manager, fontSize]);

  return (
    <div className="choice-document _bigSection">
      <h1 className="choice-document__label _title">Договір</h1>
      {/* Договір */}
      <section className="choice-document__document _section">
        <h2 className="_red">Оберіть необхідний договір*</h2>
        <ListRadiobuttons
          arrValues={documentsListVerest}
          nameValue="documentName"
          getValueRadiobutton={setNameDoc}
          indexDefaultValue={0}
        />
      </section>

      {/* Номер Договору */}
      <section className="choice-document__number _section">
        <h2 className="_red">Введіть номер документу*</h2>
        <input
          type="text"
          placeholder="Введіть номер договору"
          className=" _input"
          value={mainInfoDocState.numberDocument}
          required
          onChange={(event) =>
            setMainInfoDocState({ ...mainInfoDocState, numberDocument: event.target.value })
          }
        />
      </section>

      {/* Дата Договору */}
      <section className="choice-document__date _section">
        <h2 className="_red">Оберіть дату документу*</h2>
        <input
          type="date"
          placeholder="Введіть дату договору"
          className=" _input"
          value={String(mainInfoDocState.dataDocument)}
          onChange={(event) =>
            setMainInfoDocState({ ...mainInfoDocState, dataDocument: event.target.value })
          }
        />
      </section>

      {/* Порядок оплати */}
      <section className="choice-document__payment _section">
        <h2 className="_red">Порядок оплати товару*</h2>
        <ListRadiobuttons
          arrValues={paymentTerm}
          nameValue="documentPayment"
          getValueRadiobutton={setPayDocument}
          indexDefaultValue={2}
          addValue={true}
          placeholder="Кількість календарних днів"
        />
      </section>

      {/* Менеджер */}
      <section className="choice-document__manager _section">
        <h2>Менеджер по роботі з контрагентом</h2>
        <select
          name="manager"
          id="manager"
          title="manager"
          className="_select"
          onChange={(event) => setManager(event.target.value)}
        >
          <option value="">Оберіть менеджера</option>
          {managers.map((manager, index) => (
            <option value={manager} key={manager + index}>
              {manager}
            </option>
          ))}
        </select>
      </section>

      {/* Розмір шрифту */}
      <section className="choice-document__manager _section">
        <h2>Розмір шрифту</h2>
        <select
          name="fontsize"
          id="fontsize"
          title="fontsize"
          className="_select"
          onChange={(event) => setFontSize(+event.target.value)}
          defaultValue={9}
          value={fontSize}
        >
          {fontSizes.map((size, index) => (
            <option value={size} key={size + index}>
              {size}
            </option>
          ))}
        </select>
      </section>
    </div>
  );
};

export default MainInfoDocumentSection;
