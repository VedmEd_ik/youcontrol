import React from 'react';
import { useSelector } from 'react-redux';
import { useLocation } from 'react-router-dom';
import { useReactToPrint } from 'react-to-print';
import { fetchExtractEDR } from 'src/redux/counterparty/createAsyncThunks';
import {
  checkedCompanyCodeSelector,
  extractEDRSelector,
  statusCheckedCompanySelector,
  statusExtractEDRSelector,
} from 'src/redux/counterparty/selectors';
import { useAppDispatch } from 'src/redux/store';
import { RotatingLines } from 'react-loader-spinner';

import './NavPanel.scss';
import ReactPDF from '@react-pdf/renderer';

type NavPanelProps = {
  refComponentToPrint: React.MutableRefObject<HTMLElement>;
  documenTitleToPrint: string;
};

const NavPanel: React.FC<NavPanelProps> = ({ refComponentToPrint, documenTitleToPrint }) => {
  const companyCode = useSelector(checkedCompanyCodeSelector);
  const statusCheckedCompany = useSelector(statusCheckedCompanySelector);
  const extractEDRLink = useSelector(extractEDRSelector);
  const statusExtractEDRLink = useSelector(statusExtractEDRSelector);

  const dispatch = useAppDispatch();

  const onPrint = useReactToPrint({
    content: () => refComponentToPrint.current,
    documentTitle: documenTitleToPrint,
  });

  const getExtractEDR = () => {
    dispatch(fetchExtractEDR(companyCode));
  };

  const { pathname } = useLocation();

  return (
    <section className="output-data__nav">
      <div className="output-data__nav-text _subtitle">Згенеровано наступний документ</div>
      <div className="output-data__nav-actions actions">
        {pathname === '/check-company' && statusCheckedCompany === 'success' ? (
          !extractEDRLink ? (
            statusExtractEDRLink === 'loading' ? (
              <RotatingLines width="20" strokeColor="black" />
            ) : (
              <button type="button" onClick={getExtractEDR} className="actions__print">
                Сформувати витяг
              </button>
            )
          ) : (
            <a href={extractEDRLink} className="actions__print" target="_blank">
              Витяг сформовано!<br></br> Завантажити
            </a>
          )
        ) : (
          ''
        )}
        <button type="button" className="actions__print _icon-print-2" onClick={onPrint}>
          Друк
        </button>
      </div>
    </section>
  );
};

export default NavPanel;
