import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { regions } from 'src/assets/data';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { PassportType } from 'src/redux/counterparty/types';
import { useAppDispatch } from 'src/redux/store';

interface CounterpartyPassportProps {}

type Passport = {
  isPassport: boolean;
  passport: PassportType;
};

const CounterpartyPassport: React.FC<CounterpartyPassportProps> = ({}) => {
  const [passportState, setPassportState] = useState<Passport>({
    isPassport: false,
    passport: {
      number: '',
      organName: '',
      organRegion: '',
      date: '',
      passportType: '',
      status: null,
    },
  });

  const dispatch = useAppDispatch();

  useEffect(() => {
    if (
      passportState.passport.passportType === 'passportTypeBook' &&
      passportState.passport.number &&
      passportState.passport.date &&
      passportState.passport.organName &&
      passportState.passport.organRegion
    ) {
      dispatch(
        setCounterParty({
          passport: {
            status: true,
            number: passportState.passport.number,
            date: passportState.passport.date,
            organName: passportState.passport.organName,
            organRegion: passportState.passport.organRegion,
            passportType: passportState.passport.passportType,
          },
        }),
      );
    } else if (
      passportState.passport.passportType === 'passportTypeID' &&
      passportState.passport.number &&
      passportState.passport.date &&
      passportState.passport.organName
    ) {
      dispatch(
        setCounterParty({
          passport: {
            status: true,
            number: passportState.passport.number,
            date: passportState.passport.date,
            organName: passportState.passport.organName,
            organRegion: '',
            passportType: passportState.passport.passportType,
          },
        }),
      );
    } else {
      dispatch(
        setCounterParty({
          passport: {
            status: null,
            number: '',
            date: '',
            organName: '',
            organRegion: '',
            passportType: '',
          },
        }),
      );
    }
  }, [passportState]);

  useEffect(() => {
    if (passportState.passport.passportType === 'passportTypeID') {
      setPassportState({
        ...passportState,
        passport: { ...passportState.passport, organRegion: '' },
      });
    }
  }, [passportState.passport.passportType]);

  useEffect(() => {
    if (passportState.isPassport) {
      setPassportState({
        ...passportState,
        passport: { ...passportState.passport, passportType: 'passportTypeBook' },
      });
    } else {
      setPassportState({
        ...passportState,
        passport: {
          number: '',
          organName: '',
          organRegion: '',
          date: '',
          passportType: '',
          status: null,
        },
      });
    }
  }, [passportState.isPassport]);

  const passportView = () => {
    if (passportState.isPassport && passportState.passport.passportType) {
      switch (passportState.passport.passportType) {
        case 'passportTypeBook':
          return (
            <>
              <input
                value={passportState.passport.number}
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, number: event.target.value },
                  })
                }
                type="text"
                className="choise-counterparty__passport-number _input"
                placeholder="Серія та номер"
              />
              <h2>Орган, яким видано паспорт</h2>
              <input
                value={passportState.passport.organName}
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, organName: event.target.value },
                  })
                }
                type="text"
                className="choise-counterparty__passport-organ _input"
                placeholder="Н-д, Дунаєвецьким РВ УМВС"
              />

              <select
                aria-label="select region organ"
                name="select-organ-regoin"
                className="choise-counterparty__select-organ _select"
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, organRegion: event.target.value },
                  })
                }
              >
                {Object.keys(regions).map((region, index) => {
                  return (
                    <option value={regions[region]} key={regions[region] + index}>
                      {region}
                    </option>
                  );
                })}
              </select>
              <h2>Дата видачі</h2>
              <input
                value={passportState.passport.date}
                type="date"
                className="choise-counterparty__passport-date _input"
                placeholder="Коли виданий"
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, date: event.target.value },
                  })
                }
              />
            </>
          );
        case 'passportTypeID':
          return (
            <>
              <input
                value={passportState.passport.number}
                type="text"
                className="choise-counterparty__passport-number _input"
                placeholder="Номер"
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, number: event.target.value },
                  })
                }
              />
              <h2>Орган, яким видано паспорт</h2>
              <input
                value={passportState.passport.organName}
                type="text"
                className="choise-counterparty__passport-organ _input"
                placeholder="Н-д, 2541"
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, organName: event.target.value },
                  })
                }
              />

              <h2>Дата видачі</h2>
              <input
                value={passportState.passport.date}
                type="date"
                className="choise-counterparty__passport-date _input"
                placeholder="Коли виданий"
                onChange={(event) =>
                  setPassportState({
                    ...passportState,
                    passport: { ...passportState.passport, date: event.target.value },
                  })
                }
              />
            </>
          );
        default:
          return '';
      }
    }
  };

  return (
    <section className="choise-counterparty__passport _section">
      <label>
        Додати паспортні дані?
        <input
          type="checkbox"
          value={''}
          name="addBank"
          className="check-box"
          onChange={(event) =>
            setPassportState({ ...passportState, isPassport: !passportState.isPassport })
          }
        />
      </label>

      {passportState.isPassport && (
        <div className="choise-counterparty__passport-type horizontal-radiobattons">
          <label>
            <input
              type="radio"
              value="passportTypeBook"
              name="passportType"
              className="radio"
              defaultChecked
              onChange={(event) =>
                setPassportState({
                  ...passportState,
                  passport: { ...passportState.passport, passportType: event.target.value },
                })
              }
            />
            <span className="label">Паспорт-книжка</span>
          </label>
          <label>
            <input
              type="radio"
              value="passportTypeID"
              name="passportType"
              className="radio"
              onChange={(event) =>
                setPassportState({
                  ...passportState,
                  passport: { ...passportState.passport, passportType: event.target.value },
                })
              }
            />
            <span className="label">ID-CARD</span>
          </label>
        </div>
      )}

      {passportView()}
    </section>
  );
};

export default CounterpartyPassport;
