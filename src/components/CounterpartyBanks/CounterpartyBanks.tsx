import React, { useEffect, useState } from 'react';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { useAppDispatch } from 'src/redux/store';

interface CounterpartyBanksProps {}
type BanksType = {
  accBank1: string;
  nameBank1: string;
  addBank: boolean;
  accBank2?: string;
  nameBank2?: string;
};

const CounterpartyBanks: React.FC<CounterpartyBanksProps> = ({}) => {
  const [banks, setBanks] = useState<BanksType>({
    accBank1: '',
    nameBank1: '',
    addBank: false,
    accBank2: '',
    nameBank2: '',
  });

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      setCounterParty({
        bankDetails: {
          bank1: {
            'bank-account': banks.accBank1,
            bank: banks.nameBank1,
          },
          bank2: {
            'bank-account': banks.accBank2,
            bank: banks.nameBank2,
          },
        },
      }),
    );
  }, [banks]);

  useEffect(() => {
    if (!banks.addBank) {
      setBanks({
        ...banks,
        nameBank2: '',
        accBank2: '',
      });
    }
  }, [banks.addBank]);

  return (
    <section className="choise-counterparty__banks _section">
      <h2 className="choise-counterparty__banks-bank">Введіть банківські реквізити контрагента</h2>
      <input
        value={banks.accBank1}
        type="text"
        className="choise-counterparty__banks-bank-account _input"
        placeholder="IBAN: UA..."
        onChange={(event) => setBanks({ ...banks, accBank1: event.target.value })}
      />
      <input
        value={banks.nameBank1}
        type="text"
        placeholder="Назва банку"
        className="_input"
        onChange={(event) => setBanks({ ...banks, nameBank1: event.target.value })}
      />
      <label>
        Додати ще один банк?
        <input
          type="checkbox"
          name="addBank"
          className="check-box"
          onChange={() => setBanks({ ...banks, addBank: !banks.addBank })}
        />
      </label>

      {banks.addBank && (
        <>
          <input
            value={banks.accBank2}
            type="text"
            className="choise-counterparty__banks-bank-account _input"
            placeholder="IBAN: UA..."
            onChange={(event) => setBanks({ ...banks, accBank2: event.target.value })}
          />
          <input
            value={banks.nameBank2}
            type="text"
            className="choise-counterparty__banks-bank-name _input"
            placeholder="Назва банку"
            onChange={(event) => setBanks({ ...banks, nameBank2: event.target.value })}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyBanks;
