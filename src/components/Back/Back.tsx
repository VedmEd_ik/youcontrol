import React from 'react';
import { useNavigate } from 'react-router-dom';
import './Back.scss';

const Back: React.FC = () => {
  const navigate = useNavigate();

  const handleClickBackButton = () => {
    navigate(-1);
  };

  return (
    <section className="back">
      <button
        type="button"
        onClick={handleClickBackButton}
        className="back__arrow _icon-back-arrow"
      >
        Назад
      </button>
    </section>
  );
};

export default Back;
