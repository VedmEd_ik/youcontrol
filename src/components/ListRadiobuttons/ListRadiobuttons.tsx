import React, { useEffect, useState, useRef } from 'react';
import './ListRadiobuttons.scss';

interface ListRadiobuttonsProps {
  arrValues: string[];
  nameValue: string;
  addValue?: boolean;
  placeholder?: string;
  getValueRadiobutton: (value: string) => void;
  indexDefaultValue?: number;
}

const ListRadiobuttons: React.FC<ListRadiobuttonsProps> = ({
  arrValues,
  addValue = false,
  nameValue,
  placeholder,
  getValueRadiobutton,
  indexDefaultValue,
}) => {
  const [checkedValue, setCheckedValue] = useState('');
  const [checkedAddValue, setCheckedAddValue] = useState('');
  const [focusAddValue, setFocusAddValue] = useState(false);

  const addValueRadioRef = useRef();

  useEffect(() => {
    if (checkedValue !== '') {
      setCheckedAddValue('');
      getValueRadiobutton(checkedValue);
    } else if (checkedAddValue !== '') {
      setCheckedValue('');
      getValueRadiobutton(checkedAddValue);
    }
  }, [checkedValue, checkedAddValue]);

  const onBlurHandler = () => {
    setFocusAddValue(false);
  };

  const onFocusHandler = () => {
    setFocusAddValue(true);
    setCheckedValue('');
    if (addValueRadioRef.current) {
      (addValueRadioRef.current as any).checked = true;
    }
  };

  return (
    <>
      {arrValues.map((value, index) => {
        if (index === indexDefaultValue) {
          return (
            <label key={index + value}>
              <input
                type="radio"
                value={value}
                name={nameValue}
                className="radio"
                onChange={(event) => {
                  setCheckedValue(event.target.value);
                  setCheckedAddValue('');
                }}
                defaultChecked
              />
              <span className="label">{value}</span>
            </label>
          );
        }
        return (
          <label key={index + value}>
            <input
              type="radio"
              value={value}
              name={nameValue}
              className="radio"
              onChange={(event) => {
                setCheckedValue(event.target.value);
                setCheckedAddValue('');
              }}
            />
            <span className="label">{value} </span>
          </label>
        );
      })}

      {addValue && (
        <label>
          <input
            type="radio"
            value={checkedAddValue}
            name={nameValue}
            className="radio"
            ref={addValueRadioRef}
          />
          <span className="label"></span>
          <input
            type="text"
            className="_input"
            onChange={(event) => setCheckedAddValue(event.target.value)}
            value={checkedAddValue}
            onFocus={onFocusHandler}
            onBlur={onBlurHandler}
            placeholder={placeholder}
          />
        </label>
      )}
    </>
  );
};

export default ListRadiobuttons;
