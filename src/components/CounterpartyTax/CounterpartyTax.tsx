import React, { useEffect, useState } from 'react';
import ListRadiobuttons from '../ListRadiobuttons';
import { singleGroups } from 'src/assets/data';
import { useSelector } from 'react-redux';
import { counterPartySelector } from 'src/redux/counterparty/selectors';
import { useAppDispatch } from 'src/redux/store';
import { setCounterParty } from 'src/redux/counterparty/slice';

interface CounterpartyTaxProps {}

type taxType = 'Загальна' | 'Спрощена' | null;

const CounterpartyTax: React.FC<CounterpartyTaxProps> = ({}) => {
  const [isTax, setIsTax] = useState(false);
  const [taxType, setTaxType] = useState<taxType>(null);
  const [singleGroup, setSingleGroup] = useState('');
  const [isPDV, setIsPDV] = useState(false);
  const [PDV, setPDV] = useState('');
  const [ipn, setIpn] = useState('');
  const [counterpartyTax, setCounterpartyTax] = useState('');

  const counterParty = useSelector(counterPartySelector);
  const dispatch = useAppDispatch();

  // Встановлюємо початкові значення
  useEffect(() => {
    if (isTax) {
      setTaxType('Спрощена');
      setIsPDV(false);
    } else {
      setTaxType(null);
      setIsPDV(false);
      setCounterpartyTax('');
    }
  }, [isTax]);

  // Встановлюємо початкові значенн для групи єдиного податку при переключені системи оподаткування з загальної на спрощену і навпаки
  useEffect(() => {
    taxType === 'Загальна' || taxType === null
      ? setSingleGroup('')
      : setSingleGroup(singleGroups['2']);
  }, [taxType]);

  useEffect(() => {
    if (isTax) {
      if (taxType === 'Загальна') {
        setCounterpartyTax('Платник податку на загальній системі');
      } else if (taxType === 'Спрощена') {
        setCounterpartyTax(`Платник єдиного податку ${singleGroup}`);
      }
    } else {
      setTaxType(null);
      setIsPDV(false);
      setIpn(null);
      setCounterpartyTax('');
      setPDV(null);
    }
  }, [isTax, taxType, singleGroup]);

  useEffect(() => {
    if (isPDV) {
      setPDV('із реєстрацією ПДВ');
    } else {
      setIpn('');
      setPDV('без реєстрації ПДВ');
    }
  }, [isPDV]);

  useEffect(() => {
    dispatch(
      setCounterParty({ ...counterParty, PDV: PDV, IPN: ipn, taxationSystem: counterpartyTax }),
    );
  }, [counterpartyTax, PDV, ipn]);

  return (
    <section className="choise-counterparty__tax _section">
      <label>
        Додати дані про систему оподаткування?
        <input type="checkbox" name="tax" className="check-box" onChange={() => setIsTax(!isTax)} />
      </label>

      {isTax && (
        <>
          <div className="choise-counterparty__tax-type horizontal-radiobattons">
            <label>
              <input
                type="radio"
                value="Загальна"
                name="tax"
                className="radio"
                onChange={() => setTaxType('Загальна')}
              />
              <span className="label">Загальна</span>
            </label>
            <label>
              <input
                type="radio"
                value="Спрощена"
                defaultChecked
                name="tax"
                className="radio"
                onChange={() => setTaxType('Спрощена')}
              />
              <span className="label"> Єдиний податок</span>
            </label>
          </div>

          {taxType === 'Спрощена' && (
            <>
              <div className="choise-counterparty__single-groups-title">
                Оберіть групу єдиного податку
              </div>
              <div className="choise-counterparty__single-groups horizontal-radiobattons">
                <ListRadiobuttons
                  arrValues={Object.keys(singleGroups)}
                  nameValue="singleGroup"
                  getValueRadiobutton={(event) => setSingleGroup(singleGroups[event])}
                  indexDefaultValue={1}
                />
              </div>
            </>
          )}

          <label>
            Платник ПДВ?
            <input
              type="checkbox"
              name="vat"
              className="check-box"
              onChange={() => setIsPDV(!isPDV)}
            />
          </label>
          {isPDV && (
            <label>
              ІПН:{' '}
              <input
                type="text"
                value={ipn}
                className="_input"
                onChange={(event) => setIpn(event.target.value)}
              />
            </label>
          )}
        </>
      )}
    </section>
  );
};

export default CounterpartyTax;
