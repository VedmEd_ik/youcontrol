import React, { useEffect, useState } from 'react';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { useAppDispatch } from 'src/redux/store';

const CounterpartyPostAdress: React.FC = () => {
  const [isPostAdress, setIsPostAdress] = useState(false);
  const [postAdress, setPostAdress] = useState('');

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      setCounterParty({
        postAddress: postAdress,
      }),
    );
  }, [postAdress]);

  useEffect(() => {
    if (!isPostAdress) {
      setPostAdress('');
    }
  }, [isPostAdress]);

  return (
    <section className="choise-counterparty__post-adress post-adress-counterparty _section">
      <label className="post-adress-counterparty__label-check-box">
        Додати поштову адресу?
        <input
          type="checkbox"
          name="postAdress"
          className="post-adress-counterparty__check-box check-box"
          onChange={() => setIsPostAdress(!isPostAdress)}
        />
      </label>

      {isPostAdress && (
        <>
          <input
            type="text"
            placeholder="Введіть поштову адресу"
            className="post-adress-counterparty__input _input"
            value={postAdress}
            onChange={(event) => setPostAdress(event.target.value)}
          />
        </>
      )}
    </section>
  );
};

export default CounterpartyPostAdress;
