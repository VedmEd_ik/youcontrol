import React from 'react';
import style from './Footer.module.scss';

const Footer: React.FC = () => {
  return (
    <footer className={style.footer_wrapper}>
      <div className={style.footer}>
        <section className={style.author}>
          © Едуард Врублевський, 2022-2023. Всі права захищені.
        </section>
      </div>
    </footer>
  );
};

export default Footer;
