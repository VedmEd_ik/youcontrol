import React, { useEffect, useState } from 'react';
import ListRadiobuttons from '../ListRadiobuttons';
import { firmDataVerest } from 'src/companies/Verest/verestData';
import { useAppDispatch } from 'src/redux/store';
import { setMyCompany } from 'src/redux/myCompany/slice';

interface MyCompanySectionProps {}

const MyCompanySection: React.FC<MyCompanySectionProps> = ({}) => {
  const [myCompanyState, setMyCompanyState] = useState(Object.keys(firmDataVerest)[0]);

  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(setMyCompany(firmDataVerest[myCompanyState]));
  }, [myCompanyState]);

  return (
    <div className="choice-company _bigSection">
      <h1 className="choice-company__title _title ">Ваша компанія чи ФОП</h1>

      <section className="_section">
        <h2 className="_red">Оберіть свою компанію чи ФОП*</h2>
        <ListRadiobuttons
          arrValues={Object.keys(firmDataVerest)}
          nameValue="myCompanyname"
          getValueRadiobutton={setMyCompanyState}
          indexDefaultValue={0}
        />
      </section>
    </div>
  );
};

export default MyCompanySection;
