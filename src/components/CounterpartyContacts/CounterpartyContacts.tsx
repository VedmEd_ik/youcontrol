import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { checkedCompanySelector } from 'src/redux/counterparty/selectors';
import { setCounterParty } from 'src/redux/counterparty/slice';
import { useAppDispatch } from 'src/redux/store';

interface CounterpartyContactsProps {}

type ContactsType = {
  phone?: string;
  email?: string;
  addPhone?: string;
  addEmail?: string;
  addedPhone: boolean;
  addedEmail: boolean;
};

const CounterpartyContacts: React.FC<CounterpartyContactsProps> = ({}) => {
  const checkedCompany = useSelector(checkedCompanySelector);

  const [contactsState, setContactsState] = useState<ContactsType>({
    phone: checkedCompany?.registry?.phones ? checkedCompany?.registry?.phones.join(', ') : '',
    email: checkedCompany?.registry?.email ? checkedCompany?.registry?.email : '',
    addedPhone: false,
    addedEmail: false,
  });

  const dispatch = useAppDispatch();

  useEffect(() => {
    const phones = checkedCompany?.registry?.phones;

    if (phones && phones.length > 0) {
      setContactsState({ ...contactsState, phone: phones.join() });
    }
  }, [checkedCompany]);

  useEffect(() => {
    !contactsState.addedPhone && setContactsState({ ...contactsState, addPhone: '' });
  }, [contactsState.addedPhone]);

  useEffect(() => {
    !contactsState.addedEmail && setContactsState({ ...contactsState, addEmail: '' });
  }, [contactsState.addedEmail]);

  useEffect(() => {
    dispatch(
      setCounterParty({
        contacts: {
          phone: contactsState.addPhone
            ? `${contactsState.phone}, ${contactsState.addPhone}`
            : contactsState.phone,
          email: contactsState.addEmail
            ? `${contactsState.email}, ${contactsState.addEmail}`
            : contactsState.email,
        },
      }),
    );
  }, [contactsState]);

  return (
    <section className="choise-counterparty__contacts _section">
      <h2 className="choise-counterparty__contacts-label ">Контактні дані</h2>

      <div className="choise-counterparty__contacts-container">
        <input
          type="tel"
          name="phone"
          className="choise-counterparty__contacts-tel _input"
          placeholder="Введіть номер телефону"
          value={contactsState.phone}
          onChange={(event) =>
            setContactsState({
              ...contactsState,
              phone: event.target.value,
            })
          }
        />

        <label
          className={!contactsState.addedPhone ? '_icon-plus_circle' : '_icon-minus-on-circle'}
        >
          {''}
          <input
            type="checkbox"
            onChange={() =>
              setContactsState({ ...contactsState, addedPhone: !contactsState.addedPhone })
            }
          />
        </label>
      </div>

      {contactsState.addedPhone && (
        <input
          type="tel"
          name="phone"
          className="choise-counterparty__contacts-tel _input"
          placeholder="Введіть номер телефону"
          value={contactsState.addPhone}
          onChange={(event) =>
            setContactsState({
              ...contactsState,
              addPhone: event.target.value,
            })
          }
        />
      )}

      <div className="choise-counterparty__contacts-container">
        <input
          type="email"
          name="email"
          className="choise-counterparty__contacts-email _input"
          placeholder="Введіть електронну адресу"
          value={contactsState.email}
          onChange={(event) =>
            setContactsState({
              ...contactsState,
              email: event.target.value,
            })
          }
        />

        <label
          className={!contactsState.addedEmail ? '_icon-plus_circle' : '_icon-minus-on-circle'}
        >
          {''}
          <input
            type="checkbox"
            onChange={() =>
              setContactsState({ ...contactsState, addedEmail: !contactsState.addedEmail })
            }
          />
        </label>
      </div>
      {contactsState.addedEmail && (
        <input
          type="email"
          name="email"
          className="choise-counterparty__contacts-email _input"
          placeholder="Введіть електронну адресу"
          value={contactsState.addEmail}
          onChange={(event) =>
            setContactsState({
              ...contactsState,
              addEmail: event.target.value,
            })
          }
        />
      )}
    </section>
  );
};

export default CounterpartyContacts;
