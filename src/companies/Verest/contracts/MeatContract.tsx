import React from 'react';
import { Page, Text, Document, View, Image } from '@react-pdf/renderer';
import { MyCompanyType } from 'src/@types/types';
import { CounterPartyType } from 'src/redux/counterparty/types';
import { DocumentDataType } from 'src/redux/document/types';
import {
  capitalize,
  documentSignerBasis,
  formatDate,
  genQrCode,
  parseNameDocument,
  paymentProcedure,
} from 'src/utils/functions';
import { styles } from '../styles';
import { StyleSheet } from '@react-pdf/renderer';

type MeatContractProps = {
  payment: string;
  myCompany: MyCompanyType;
  counterparty: CounterPartyType;
  dataDocument: DocumentDataType;
  fontSize: number;
};
const MeatContract: React.FC<MeatContractProps> = ({
  payment,
  myCompany,
  counterparty,
  dataDocument,
  fontSize,
}) => {
  const { nameDocument, document } = parseNameDocument(dataDocument.name);
  console.log('Рендер договору');
  // Дані для qr-коду
  const textForQRCode = `${document} ${nameDocument} № ${dataDocument.number} від ${
    dataDocument.date
  } року, укладений між ${myCompany.shortName} та ${counterparty.shortName}.${
    dataDocument.manager && ` Відповідальний за роботу з контрагентом: ${dataDocument.manager}`
  }`;

  const fontStyles = StyleSheet.create({
    mainStyles: {
      height: '100%',
      paddingTop: 30,
      paddingBottom: 40,
      paddingLeft: 50,
      paddingRight: 30,
      fontFamily: 'Roboto',
      fontSize: fontSize,
    },
  });

  return (
    <Document>
      <Page style={fontStyles.mainStyles} size="A4">
        <View style={styles.wrap}>
          {/* QR-код */}
          <View style={styles.qrCode}>
            <Image src={genQrCode(textForQRCode)} style={styles.qrCode}></Image>
          </View>
          {/* Назва та номер договору */}
          <Text style={styles.title}>
            {document} № {dataDocument.number}
          </Text>
          <Text style={styles.title}>{nameDocument}</Text>
          {/* Місце та дата укладення */}
          <View style={styles.date}>
            <Text style={styles.text}>с.Гірчична</Text>
            <Text style={styles.text}>{formatDate(dataDocument.date)} року</Text>
          </View>
          {/* Преамбула */}
          <Text style={styles.text}>
            <Text style={styles._bold}>{myCompany.fullName.toUpperCase()}</Text>, від імені якого
            діє {myCompany.signer.role.toLowerCase()} {myCompany.signer.name} (документ на підставі
            якого діє - {myCompany.signer.basis}) (надалі по тексту - "Постачальник"), та
          </Text>
          <Text style={[styles.text, styles._marginTop]}>
            <Text style={styles._bold}>{counterparty.fullName.toUpperCase()}</Text>, від імені якого
            діє {counterparty.signer.role.toLowerCase()} {counterparty.signer.name} (документ, на
            підставі якого він діє - {documentSignerBasis(counterparty)}) (надалі по тексту -
            "Покупець", разом - "Сторони", а кожна окремо - "Сторона"), уклали цей{' '}
            {dataDocument.name} № {dataDocument.number} (надалі по тексту - "Договір") про наступне:
          </Text>
          {/* 1. ПРЕДМЕТ ДОГОВОРУ */}
          <Text style={styles.subtitle}>1. ПРЕДМЕТ ДОГОВОРУ</Text>
          <Text style={[styles.text, styles.textItem]}>
            1.1. Постачальник зобов'язується передати у власність Покупця м'ясні та ковбасні вироби,
            а також іншу продукцію тваринного походження (свіже, охолодженне чи заморожене м'ясо
            свинини, яловичини, курки та інші напівфабрикати) (надалі по тексту "Продукція"), а
            Покупець прийняти та оплатити її згідно з умовами цього Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            1.2. Поставка Продукції здійснюється окремими партіями. Найменування, ціна та кількість
            товарів, що підлягають поставці, їх часткове співвідношення (асортимент, сортамент,
            номенклатура) за сортами, групами, підгрупами, видами, марками, типами, розмірами тощо
            зазначаються у видатковій накладній, яка за згодою Сторін є одночасно і специфікацією в
            розумінні ст. 266 Господарського кодексу України.
          </Text>
          {/* 2. ЯКІСТЬ ТОВАРУ ТА УПАКОВКА */}
          <Text style={styles.subtitle}>2. ЯКІСТЬ ТОВАРУ ТА УПАКОВКА</Text>
          <Text style={[styles.text, styles.textItem]}>
            2.1. Якість Продукції, що передається, повинна відповідати вимогам законодавства України
            та нормативним документам (ДСТУ, ТУ тощо).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.2. Постачальник забезпечує таке пакування товару, яке необхідне для запобігання його
            пошкодженню або псуванню під час транспортування до кінцевого пункту призначення.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.3. Після прийняття Продукції Покупець зобов'язаний дотримуватися правил її
            транспортування, зберігання та реалізації, встановлених ДСТУ, ТУ та чинним
            законодавством України для даного виду продукції. При порушенні цих правил Постачальник
            не відповідає за якість продукції та звільняється від будь-якої відповідальності,
            пов'язаної із поставкою неякісної продукції.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            2.4. Покупець має право на заміну отриманої партії Продукції чи її частини лише у
            випадку, якщо її якість не відповідає вимогам Договору, що підтверджено документально.
          </Text>
          {/* 3. ПОРЯДОК ЗАМОВЛЕННЯ ПРОДУКЦІЇ ПОКУПЦЕМ */}
          <Text style={styles.subtitle}>3. ПОРЯДОК ЗАМОВЛЕННЯ ПРОДУКЦІЇ ПОКУПЦЕМ</Text>
          <Text style={[styles.text, styles.textItem]}>
            3.1. Поставка Продукції здійснюється окремими партіями, відповідно до наданого Покупцем
            та схваленого Постачальником замовлення на поставку Продукції.{' '}
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.2. Замовлення на поставку відповідної партії Продукції може подаватися Покупцем в
            письмовій формі, або в електронній формі на електронну пошту Постачальника, або через
            платформу електронної комерції EDI, або через торгового представника Постачальника. В
            замовленні обов’язково повинно бути вказано: кількість Продукції; вид та найменування
            Продукції; строк поставки Продукції.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.3. Замовлення на поставку відповідної партії Продукції Покупець подає не менш ніж за
            24 години до поставки Товару. Замовлення подані з пропущенням цього строку Постачальник
            має право не розглядати.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.4. Замовлення приймаються Постачальником з 8:00 до 14:00 години з понеділка по суботу
            (неділя вихідний). Замовлення подані після 14:00 години вважаються такими, що подані
            наступного дня.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            3.5. У випадку неможливості виконати Замовлення, Постачальник протягом 6 годин з моменту
            його отримання сповіщає про це Покупця на електронну адресу або в усній формі по
            телефону, при цьому він не несе відповідальності за непоставку Товару.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            {' '}
            3.6. Сторони домовилися, що у випадку якщо Постачальник здійснив поставку Продукції без
            погодженого замовлення, а Покупець прийняв її, то вважається, що поставка відбулася
            відповідно до умов Договору, а тому в Покупця виникає обов’язок оплатити її.
          </Text>
          {/* 4. УМОВИ ПОСТАВКИ ПРОДУКЦІЇ */}
          <Text style={styles.subtitle}>4. УМОВИ ПОСТАВКИ ПРОДУКЦІЇ</Text>
          <Text style={[styles.text, styles.textItem]}>
            4.1. За домовленістю Сторін (усною чи письмовою) доставка Продукції може здійснюватися
            як транспортом Покупця так і транспортом Постачальника.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.2. У випадку поставки товару транспортом Постачальника, Покупець зобов'язаний надати
            Постачальнику офіційного листа з переліком пунктів доставки та їх адресами. В
            протилежному випадку Постачальник має право не поставляти Товар.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.3. Якщо Сторони не домовилися про інше, то усі витрати, пов'язані з транспортуванням
            та доставкою Продукції, здійснюються за рахунок Сторони, яка виконує таке
            транспортування.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            4.4. Одночасно з передачею кожної партії Продукції Постачальник передає Покупцеві усі
            необхідні товаросупровідні документи.
          </Text>
          {/* 5. ПОРЯДОК ПРИЙМАННЯ-ПЕРЕДАЧІ ПРОДУКЦІЇ */}
          <Text style={styles.subtitle}>5. ПОРЯДОК ПРИЙМАННЯ-ПЕРЕДАЧІ ПРОДУКЦІЇ</Text>
          <Text style={[styles.text, styles.textItem]}>
            5.1. Право власності та всі ризики стосовно Продукції переходить в момент фактичної
            передачі Продукції Постачальником Покупцю. Моментом фактичного отримання Продукції є
            момент підписання Покупцем видаткової накладної.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.2. Датою поставки і переходу права власності та всіх ризиків є:
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.2.1. при передачі Продукції на складі Постачальника або Покупця – дата підписання
            Покупцем видаткової накладної, яка видається Постачальником;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.2.2. при передачі Продукції транспортній організації для доставки Покупцю - дата
            товарно-транспортної накладної щодо передачі Продукції зазначеному перевізнику.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.3. Покупець зобов'язаний перевірити зовнішній вигляд та кількість збірної тари
            (упаковок) у момент приймання Продукції. В протилежному випадку претензії щодо
            цілісності, неушкодженості та інші претензії, що стосуються зовнішнього вигляду та
            кількості збірної тари (упаковки) не розглядаються.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.4. У разі виявлення неякісної або бракованої Продукції Покупець має право на заміну
            такої Продукції. Для цього він повинен одразу після виявлення такої Продукції але не
            пізніше 1-го календарного дня з моменту її отримання, направити Постачальнику
            обґрунтовану письмову заяву про заміну такої Продукції на якісну. Постачальник
            зобов’язаний розглянути таку заяву протягом 2-х календарних днів і прийняти відповідне
            рішення (про заміну Товару або про відмову заміни) про що повідомляє Покупця. У разі
            прийняття рішення про заміну Товару Постачальник зобов'язаний здійснити таку заміну
            протягом 2 (двох) календарних днів з моменту повідомлення Покупця про прийняте рішення.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.5. Ніякі претензії, заявлені по будь-якій Продукції в замовленій партії, не можуть
            служити підставою для відмови Покупцем від прийомки та/або оплати:
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.5.1. Продукції інших асортиментних найменувань даної партії, по яких не заявлено
            претензій;
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.5.2. по інших поставках, що здійснені, здійснюються або будуть здійснені в майбутньому
            відповідності до умов даного Договору,
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.5.3. а також не дають права Покупцю вчиняти утримання будь-яких сум коштів з належних
            Постачальнику платежів та/або не виконувати інші умови даного Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.6. Товари, поставлені згідно із замовленням Покупця та з дотриманням вимог даного
            Договору, поверненню не підлягають.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            5.7. Підписанням цього Договору Покупець свідчить, що особи, які будуть підписувати
            накладні, товарно-транспортні накладні про приймання Продукції від імені Покупця, мають
            відповідні повноваження на таке підписання та приймання Продукції в інтересах та на
            користь Покупця. Покупець не має права не сплачувати за поставлений Постачальником Товар
            та відмовитися від його приймання на підставі відсутності відповідних повноважень у
            особи, яка підписала від його імені відповідні документи і прийняла Товар в інтересах і
            на користь Покупця.
          </Text>
          {/* 6. ЦІНА НА ПРОДУКЦІЮ ТА ПОРЯДОК РОЗРАХУНКІВ */}
          <Text style={styles.subtitle}>6. ЦІНА НА ПРОДУКЦІЮ ТА ПОРЯДОК РОЗРАХУНКІВ</Text>
          <Text style={[styles.text, styles.textItem]}>
            6.1. Ціна на Продукцію встановлюється Постачальником в національній валюті України –
            гривнях і вказується у видаткових накладних та/або рахунках. Вартість тари, упакування
            та маркування Продукції внесене в її ціну.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.2. Загальна вартість Договору складається з вартості всіх партій Продукції,
            поставлених Покупцю за цим Договором.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.3. Розрахунки за кожну поставлену партію Продукції можуть здійснюватися Покупцем як у
            готівковій, так і в безготівковій формі.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.4. У випадку безготівкової оплати Покупець зобов'язаний при оплаті Продукції в
            платіжному документі (дорученні, квитанції тощо) вказувати номер і дату видаткової
            накладної або рахунку, на підставі якої здійснюється оплата.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.5. Датою оплати продукції вважається дата їх отримання Постачальником.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.6. Покупець зобов'язаний оплатити Продукцію відповідно до рахунку та/або накладної{' '}
            <Text style={styles._bold}>{paymentProcedure(payment, 'товар')}.</Text>
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.7. У випадку наявності простроченої заборгованості Покупця перед Постачальником,
            останній має право не здійснювати наступні поставки Товару до повної оплати Покупцем уже
            поставлених партій Товару та іншої заборгованості (штрафи, пеня тощо). В даному випадку
            це не буде вважатися порушенням Постачальником умов даного Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.8. За рахунок коштів, сплачених Покупцем згідно з Договором, його грошові зобов'язання
            припиняють у наступній черговості: у першу чергу – штрафні санкції (штраф, пеня тощо); у
            другу чергу – прострочені платежі; у третю чергу – поточні платежі.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            6.9. Сторони мають право здійснювати розрахунки за цим Договором в інший спосіб, не
            заборонений законодавством України.
          </Text>
          {/* 7. КОНФІДЕНЦІЙНІСТЬ */}
          <Text style={styles.subtitle}>7. КОНФІДЕНЦІЙНІСТЬ</Text>
          <Text style={[styles.text, styles.textItem]}>
            7.1. Сторони погодилися, що текст Договору, які-небудь матеріали, інформація та
            відомості, які стосуються Договору, є конфіденційними та не можуть використовуватися або
            передаватися третім особам без попередньої письмової домовленості другої Сторони
            договору, крім випадків, коли така передача пов'язана з одержанням офіційних дозволів,
            документів для виконання Договору або сплати податків, інших обов'язкових платежів, а
            також у випадках, передбачених чинним законодавством України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            7.2. Укладанням цього Договору Сторони надають одна одній згоду та право на включення
            даних (персональних та/або ідентифікуючих) протилежної Сторони до баз даних, що
            необхідні для складання бухгалтерської, статистичної, фінансової, податкової звітності,
            а також інших форм звітності, моніторингу чи контролю, що оформляються та ведуться
            згідно с стандартами, які використовуються Стороною, яка отримала дані. Сторони
            підтверджують, погоджуються та гарантують, що дані (персональні та/або ідентифікуючі)
            Сторони, які стають відомі іншій Стороні в ході виконання цього Договору, можуть бути
            включені, зберігатись, оброблятись (використовуватись і т.п.) в базах даних (в тому
            числі електронних) як в ході виконання цього Договору, так і після закінчення його дії.
            Дані Сторони цього Договору, що занесені іншою Стороною до власних баз даних, не можуть
            бути використані останньою з метою або за призначенням, що не відповідають умовам цього
            Договору або вимогам чинного законодавства.
          </Text>
          {/* 8. ВІДПОВІДАЛЬНОСТІ СТОРІН */}
          <Text style={styles.subtitle}>8. ВІДПОВІДАЛЬНОСТІ СТОРІН</Text>
          <Text style={[styles.text, styles.textItem]}>
            8.1. У випадку невиконання або неналежного виконання Сторонами умов даного Договору,
            вони несуть відповідальність згідно із чинним законодавством України та цим Договором.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.2. У випадку несвоєчасної оплати Продукції згідно умов Договору Постачальник має право
            нарахувати, а Покупець зобов'язаний сплатити на користь Постачальника пеню у розмірі
            подвійної облікової ставки НБУ, що діяла у період, за який сплачується пеня, від суми
            простроченого платежу за кожен день прострочення оплати.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.3. У випадку прострочення оплати Продукції згідно умов Договору більш ніж 7 (сім)
            календарних днів Покупець зобов'язаний окрім пені сплатити на користь Постачальника
            також штраф в розмірі 10% від простроченої суми, а за прострочення більш ніж на 30
            календарних днів – додатково штраф у розмірі 50 % від простроченої суми.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.4. У разі порушення Постачальником строків поставки Товару Покупець має право
            нарахувати, а Постачальник зобов'язаний сплатити на користь Покупця пеню у розмірі
            подвійної облікової ставки НБУ, що діяла у період, за який сплачується пеня, від суми
            несвоєчасно поставленої партії Товару за кожен день прострочення поставки.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            8.5. Сплата неустойки (штрафу, пені) і відшкодування збитків, заподіяних неналежним
            виконанням зобов'язань, не звільняють Сторони від виконання зобов'язань за цим Договором
            у дійсності.
          </Text>
          {/* 9. ФОРС-МАЖОР */}
          <Text style={styles.subtitle}>9. ФОРС-МАЖОР</Text>
          <Text style={[styles.text, styles.textItem]}>
            9.1. Кожна зі Сторін не відповідає за невиконання, несвоєчасне або невідповідне
            виконання якого-небудь зобов'язання за цим Договором, якщо вищезгадане невиконання,
            несвоєчасне або невідповідне виконання обумовлене форс-мажорними обставинами.
            Форс-Мажорними визначаються, але цим список не обмежується, такі обставини: пожежі,
            урагани, повені, землетруси, епідемії, і інші стихійні лиха та техногенні катастрофи,
            повстання, ембарго, війна та воєнні дії якого-небудь типу (включаючи громадянську
            війну), окупація, мобілізація, порушення цивільного порядку, загальнонаціональні
            сутички.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            9.2. Якщо форс-мажорні обставини тривають більше ніж три місяці, кожна Сторона має право
            відмовитися від цього Договору шляхом письмового повідомлення іншої Сторони.
          </Text>
          {/* 10. ПОРЯДОК ВИРІШЕННЯ СПОРІВ */}
          <Text style={styles.subtitle}>10. ПОРЯДОК ВИРІШЕННЯ СПОРІВ</Text>
          <Text style={[styles.text, styles.textItem]}>
            10.1. Всі спори та суперечки, які виникають між Сторонами у зв'язку із цим Договором
            (його тлумаченням, виконанням, призупиненням т.д.), Сторони вирішують шляхом
            переговорів.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            10.2. Якщо Сторони не можуть дійти до згоди, то спір або суперечка, які виникають за
            Договором або у зв'язку з ним, підлягають розгляду в господарському суді відповідно до
            підсудності, встановленої законом.
          </Text>
          {/* 11. ТЕРМІН ДІЇ ДОГОВОРУ ТА ІНШІ УМОВИ */}
          <Text style={styles.subtitle}>11. ТЕРМІН ДІЇ ДОГОВОРУ ТА ІНШІ УМОВИ</Text>
          <Text style={[styles.text, styles.textItem]}>
            11.1. Договір набуває чинності з моменту його підписання і скріплення печатками сторін і
            діє до кінця календарного року, в якому його було підписано сторонами.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.2. У випадку, якщо за 30 календарних днів до закінчення терміну дії Договору жодна із
            Сторін не заявила письмово про припинення його дії, Договір вважається продовженим
            автоматично на кожен наступний календарний рік на тих же умовах.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.3. Договір припиняє свою дію у разі: закінчення терміну його дії; припинення його дії
            за згодою обох Сторін; розірвання його за вимогою однієї із Сторін, у порядку
            встановленому цим Договором; укладення нового договору стосовно такого ж предмету
            договору; в інших випадках, передбачених чинним законодавством України.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.4. В будь-якому випадку Договір продовжує свою дію до моменту виконання Сторонами
            своїх зобов’язань, що виникли під час дії Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.5. У разі виявлення бажання розірвати договір, Сторона-ініціатор не менш ніж за 15
            календарних днів до бажаної дати розірвання, надсилає іншій Стороні відповідне письмове
            повідомлення. У разі відсутності невиконаних зобов'язань Сторін за Договором, він
            припиняє свою дію після спливу 15 календарних днів з моменту направлення повідомлення
            без підписання будь-яких додаткових документів (угод, додатків тощо).
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.6. Підписанням цього Договору Сторони погоджуються, що усі Договори, предмет яких
            аналогічний предмету даного Договору та які були укладені між Сторонами до моменту
            набрання чинності цим Договором втрачають юридичну силу.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.7. Враховуючи положення ч. 2 ст. 207 Цивільного кодексу України та ст.181
            Господарського кодексу України Сторони погодили, що належно оформлені та підписані
            Сторонами копії цього Договору, рахунків-фактур, додаткових угод до нього, а також інших
            документів, що додаються до цього Договору і складають його невід’ємну частину, які
            передані між Сторонами за допомогою факсу, електронної пошти та іншими засобами
            телекомунікації, мають силу оригіналів до моменту обміну Сторонами належним чином
            оформленими оригіналами документів. Окрім цього, якщо вищезазначені документи були
            підписані електронним підписом (ЕЦП, КЕП тощо) уповноважених осіб та передані за
            допомогою засобів телекомунікації (спеціальних програм, електронної пошти тощо) , то
            вони також мають силу оригіналів.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.8. При підписанні цього договору, змін та доповнень до нього, актів та інших
            документів до цього Договору, Сторонами може бути використане факсимільне відтворення
            підпису уповноважених представників Сторін за допомогою засобів механічного копіювання.
            При цьому факсимільний підпис матиме таку ж силу, як і справжній підпис уповноваженого
            представника Сторони.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.9. Після підписання цього Договору всі попередні переговори за ним, листування,
            попередні договори, протоколи про наміри та будь-які інші усні або письмові домовленості
            Сторін з питань, що так чи інакше стосуються цього Договору, втрачають юридичну силу,
            але можуть враховуватися при тлумаченні умов цього Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.10. Які-небудь зміни та доповнення до цього Договору, у тому числі про його
            призупинення (припинення) або продовження дії договору, будуть дійсними, якщо вони
            викладені в письмовій формі та підписані уповноваженими на це представниками і
            скріплення печатками Сторін.{' '}
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.11. У випадку зміни податкового статусу будь-якої із Сторін, вона зобов’язана
            якнайшвидше повідомити про це іншу Сторону. При цьому внесення відповідних змін в цей
            Договір не обов’язкове.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.12. Сторони підтверджують, що особи, які підписали цей Договір, мають для цього
            необхідні повноваження або укладення цього Договору схвалено особою (особами), від імені
            якого (яких) діє особа, що підписала Договір, що підтверджується круглою печаткою на
            підписі.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.13. Кожна зі Сторін не має право передавати свої права за договором третій Стороні
            без письмової угоди іншої Сторони.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.14. У випадку реорганізації однієї зі Сторін, правонаступник Сторони Договору
            безпосередньо приймає на себе всі права та зобов'язання цього Договору, якщо Сторони
            додатково не вирішать інакше.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.15. Усі правовідносини, які виникають у зв'язку з виконанням умов цього Договору та
            неурегульовані ним, регламентуються нормами чинного законодавства України. У випадку,
            якщо окремі положення (умови) цього Договору у встановленому законодавством України
            порядку будуть визнані недійсними, це не спричиняє визнання недійсними інших положень
            (умов) Договору.
          </Text>
          <Text style={[styles.text, styles.textItem]}>
            11.16. Цей Договір складений при повному розумінні Сторонами його умов та термінології
            українською мовою у двох автентичних примірниках, які мають однакову юридичну силу, – по
            одному для кожної із Сторін.
          </Text>
          {/* 12. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН */}
          <Text style={styles.subtitle}>
            12. АДРЕСА, ПОДАТКОВИЙ СТАТУС, ПЛАТІЖНІ РЕКВІЗИТИ, ПІДПИСИ СТОРІН
          </Text>
          {/* Реквізити сторін */}
          <View style={styles.detailsContainer}>
            {/* Реквізити моєї компанії */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ПОСТАЧАЛЬНИК</Text>
              </View>
              <Text style={styles.detailTitle}>{myCompany.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична/фактична адреса:</Text>
                <Text style={styles.detailsText}>{myCompany.address}</Text>
              </View>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{myCompany.code}</Text>
              </View>
              {myCompany.passport.status && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                  {myCompany.passport.passportType === 'passportTypeBook' ? (
                    <Text>
                      {myCompany.passport.number}, виданий {myCompany.passport.organName} в{' '}
                      {myCompany.passport.organRegion} {formatDate(myCompany.passport.date)} року
                    </Text>
                  ) : (
                    <Text>
                      ID-картка № {myCompany.passport.number}, виданий{' '}
                      {formatDate(myCompany.passport.date)} року органом{' '}
                      {myCompany.passport.organName}
                    </Text>
                  )}
                </View>
              )}

              {myCompany.taxationSystem && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                  <Text style={styles.detailsText}>
                    {myCompany.taxationSystem} {myCompany.PDV}
                  </Text>
                </View>
              )}

              {myCompany.IPN && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>ІПН: </Text>
                  <Text style={styles.detailsText}>{myCompany.IPN}</Text>
                </View>
              )}

              {myCompany.contacts.phone && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Тел.: </Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.phone}</Text>
                </View>
              )}

              {myCompany.contacts.email && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>E-mail:</Text>
                  <Text style={styles.detailsText}>{myCompany.contacts.email}</Text>
                </View>
              )}
              {myCompany.bankDetails && (
                <>
                  {myCompany.bankDetails.bank1 &&
                    myCompany.bankDetails.bank1['bank-account'] &&
                    myCompany.bankDetails.bank1.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank1['bank-account']} в{' '}
                            {myCompany.bankDetails.bank1.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                  {myCompany.bankDetails.bank2 &&
                    myCompany.bankDetails.bank2['bank-account'] &&
                    myCompany.bankDetails.bank2.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {myCompany.bankDetails.bank2['bank-account']} в{' '}
                            {myCompany.bankDetails.bank2.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                </>
              )}
            </View>

            {/* Реквізити контрагента */}
            <View style={styles.detailsFirm}>
              <View style={styles.firmType}>
                <Text style={styles.detailTitle}>ПОКУПЕЦЬ</Text>
              </View>
              <Text style={styles.detailTitle}>{counterparty.shortName.toUpperCase()}</Text>

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Юридична адреса:</Text>
                <Text style={styles.detailsText}>{counterparty.address}</Text>
              </View>
              {counterparty.postAddress && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Поштова адреса:</Text>
                  <Text style={styles.detailsText}>{counterparty.postAddress}</Text>
                </View>
              )}

              <View style={styles.detailsInfoBlock}>
                <Text style={styles.detailsSubtitle}>Код ЄДРПОУ: </Text>
                <Text style={styles.detailsText}>{counterparty.code}</Text>
              </View>
              {counterparty.passport.status && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Паспорт: </Text>
                  {counterparty.passport.passportType === 'passportTypeBook' ? (
                    <Text>
                      {counterparty.passport.number}, виданий {counterparty.passport.organName} в{' '}
                      {counterparty.passport.organRegion} {formatDate(counterparty.passport.date)}{' '}
                      року
                    </Text>
                  ) : (
                    <Text>
                      ID-картка № {counterparty.passport.number}, виданий{' '}
                      {formatDate(counterparty.passport.date)} року органом{' '}
                      {counterparty.passport.organName}
                    </Text>
                  )}
                </View>
              )}

              {counterparty.taxationSystem && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Система оподаткування: </Text>
                  <Text style={styles.detailsText}>
                    {counterparty.taxationSystem} {counterparty.PDV}
                  </Text>
                </View>
              )}

              {counterparty.IPN && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>ІПН: </Text>
                  <Text style={styles.detailsText}>{counterparty.IPN}</Text>
                </View>
              )}

              {counterparty.contacts.phone && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>Тел.: </Text>
                  <Text style={styles.detailsText}>{counterparty.contacts.phone}</Text>
                </View>
              )}

              {counterparty.contacts.email && (
                <View style={styles.detailsInfoBlock}>
                  <Text style={styles.detailsSubtitle}>E-mail:</Text>
                  <Text style={styles.detailsText}>{counterparty.contacts.email}</Text>
                </View>
              )}

              {counterparty.bankDetails && (
                <>
                  {counterparty.bankDetails.bank1 &&
                    counterparty.bankDetails.bank1['bank-account'] &&
                    counterparty.bankDetails.bank1.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {counterparty.bankDetails.bank1['bank-account']} в{' '}
                            {counterparty.bankDetails.bank1.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                  {counterparty.bankDetails.bank2 &&
                    counterparty.bankDetails.bank2['bank-account'] &&
                    counterparty?.bankDetails?.bank2.bank && (
                      <View style={styles.detailsInfoBlock}>
                        <View style={styles.bankAccount}>
                          <Text style={styles.detailsSubtitle}>IBAN: </Text>
                          <Text style={styles.detailsText}>
                            {counterparty.bankDetails.bank2['bank-account']} в{' '}
                            {counterparty.bankDetails.bank2.bank}
                          </Text>
                        </View>
                      </View>
                    )}
                </>
              )}
            </View>
          </View>
          {/* Підписи сторін */}
          <View style={[styles.detailsSignatures, styles._marginTop]}>
            {/* Підпис мого керівника */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(myCompany.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {myCompany.signer.name}
              </Text>
            </View>

            {/* Підпис контрагента */}
            <View style={styles.detailsSignature}>
              <Text style={styles.text}>{capitalize(counterparty.signer.role)}</Text>
              <Text style={styles.text}> </Text>
              <Text style={[styles.text, styles.detailsSignatureRole, styles._underLine]}>
                {counterparty.signer.name}
              </Text>
            </View>
          </View>
        </View>
        {/* Нижній колонтитул */}
        <View style={styles.footerContainer} fixed>
          <Text>Постачальник ______________</Text>
          <Text
            style={styles.pageNumber}
            render={({ pageNumber, totalPages }) => `Сторінка ${pageNumber} з ${totalPages}`}
          />
          <Text>Покупець ______________</Text>
        </View>
      </Page>
    </Document>
  );
};

export default MeatContract;
