import React, { useRef } from 'react';
import { MeatContract, PetrolContract, LivestockContract, ServicesContract } from './contracts';
import './documentStyles.scss';
import '../../styles/print.scss';
import { PDFViewer } from '@react-pdf/renderer';
import { CounterPartyType } from 'src/redux/counterparty/types';
import { DocumentDataType } from 'src/redux/document/types';
import { MyCompanyType } from 'src/@types/types';
import { useSelector } from 'react-redux';
import { fontSizeSelector } from 'src/redux/document/selectors';

type OutDocumentProps = {
  counterparty?: CounterPartyType;
  dataDocument?: DocumentDataType;
  myCompany?: MyCompanyType;
};

const OutDocumentVerest: React.FC<OutDocumentProps> = ({
  counterparty,
  dataDocument,
  myCompany,
}) => {
  const payment = dataDocument?.paymentTerm;
  const outDocumentRef = useRef<HTMLDivElement>();
  const fontSize = useSelector(fontSizeSelector);

  const document = () => {
    switch (dataDocument?.name) {
      case "Договір поставки м'ясопродуктів":
        return (
          <PDFViewer className="iframe-pdf">
            <MeatContract
              payment={payment}
              myCompany={myCompany}
              counterparty={counterparty}
              dataDocument={dataDocument}
              fontSize={fontSize}
            />
          </PDFViewer>
        );

      case 'Договір поставки нафтопродуктів':
        return (
          <PDFViewer className="iframe-pdf">
            <PetrolContract
              payment={payment}
              myCompany={myCompany}
              counterparty={counterparty}
              dataDocument={dataDocument}
              fontSize={fontSize}
            />
          </PDFViewer>
        );
      case 'Договір поставки свиней, ВРХ':
        return (
          <PDFViewer className="iframe-pdf">
            <LivestockContract
              payment={payment}
              myCompany={myCompany}
              counterparty={counterparty}
              dataDocument={dataDocument}
              fontSize={fontSize}
            />
          </PDFViewer>
        );
      case 'Договір про надання послуг':
        return (
          <PDFViewer className="iframe-pdf">
            <ServicesContract
              payment={payment}
              myCompany={myCompany}
              counterparty={counterparty}
              dataDocument={dataDocument}
              fontSize={fontSize}
            />
          </PDFViewer>
        );
    }
  };

  return (
    <>
      <div className="output-data__document document" ref={outDocumentRef}>
        {document()}
      </div>
    </>
  );
};

export default OutDocumentVerest;
