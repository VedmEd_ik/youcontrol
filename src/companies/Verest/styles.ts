import { Font, StyleSheet } from '@react-pdf/renderer';
import RobotoRegular from '../../fonts/Roboto-Regular.ttf';
import RobotoBold from '../../fonts/Roboto-Bold.ttf';
import RobotoBlack from '../../fonts/Roboto-Black.ttf';

Font.register({
  family: 'Roboto',
  formar: 'truetype',
  fonts: [
    { src: RobotoRegular, fontWeight: 400 },
    { src: RobotoBold, fontWeight: 700 },
    { src: RobotoBlack, fontWeight: 900 },
  ],
});

export const styles = StyleSheet.create({
  qrCode: {
    width: 40,
    position: 'absolute',
    top: -10,
    left: 0,
  },
  body: {
    height: '100%',
    paddingTop: 30,
    paddingBottom: 40,
    paddingLeft: 50,
    paddingRight: 30,
    fontFamily: 'Roboto',
    fontSize: 9,
  },
  wrap: {},
  title: {
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontWeight: 700,
  },
  _bold: {
    fontFamily: 'Roboto',
    fontWeight: 700,
  },
  _marginTop: {
    marginTop: 5,
  },
  _underLine: {
    borderBottom: '1px solid black',
  },

  author: {
    textAlign: 'center',
    marginBottom: 40,
  },
  date: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 10,
  },
  subtitle: {
    marginTop: 10,
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontWeight: 700,
  },
  text: {
    textAlign: 'justify',
  },
  textItem: {
    marginBottom: 5,
  },
  image: {
    marginVertical: 15,
    marginHorizontal: 100,
  },
  header: {
    marginBottom: 20,
    textAlign: 'center',
    color: 'grey',
  },
  pageNumber: {},
  detailsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  detailsFirm: {
    width: '49%',
  },
  firmType: {
    marginTop: 10,
    marginBottom: 2,
  },
  detailTitle: {
    fontFamily: 'Roboto',
    fontWeight: 700,
    textAlign: 'center',
  },
  detailsInfoBlock: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  detailsSubtitle: {
    fontFamily: 'Roboto',
    fontWeight: 700,
  },
  detailsText: {},
  bankAccount: {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  detailsSignatures: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  detailsSignature: {
    width: '49%',
  },
  detailsSignatureRole: {
    textAlign: 'right',
  },
  footer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  footerContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    position: 'absolute',
    bottom: 15,
    left: 0,
    right: 0,
    textAlign: 'center',
    color: 'grey',
    paddingLeft: 50,
    paddingRight: 30,
  },
});
