import { Navigate } from 'react-router-dom';
import useAuth from '../hooks/use-auth';
import React from 'react';
import { DocumentsList, FirmDataType, PaymentTerm } from 'src/@types/types';

type PrivateRouteProps = {
  children?: JSX.Element;
};

const PrivateRoute: React.FC<PrivateRouteProps> = ({ children }) => {
  const { isAuth } = useAuth();

  const userData = JSON.parse(window.localStorage.getItem('userData'));

  const documentsList: DocumentsList = userData?.documentsList;
  const paymentTerm: PaymentTerm = userData?.paymentTerm;
  const firmData: FirmDataType = userData?.firmData;

  if (!isAuth || !userData) return <Navigate to="/" />;

  // Клонуємо елемент children і передаємо додаткові пропси
  const clonedChildren = React.cloneElement(children, { documentsList, firmData, paymentTerm });

  return clonedChildren;
};

export default PrivateRoute;
